#include <stdlib.h>
#include <BasicFiniteElements.hpp>
#include <FEOperator.hpp>
#include <FETools.hpp>

static char help[] = "...\n\n";

using namespace MoFEM;
using namespace Operators;

double initVal = 1;


struct FEProblem {
public:
  FEProblem(MoFEM::Interface &m_field);

  MoFEMErrorCode runAnalysis();

private:
  MoFEMErrorCode setupInput();
  MoFEMErrorCode findBlocks();
  MoFEMErrorCode setupFields();
  MoFEMErrorCode setupIntegrationRule();
  MoFEMErrorCode applyBIcondition();
  MoFEMErrorCode updateElements(boost::shared_ptr<FaceEle> &fe_ele);
  MoFEMErrorCode setupFETSsystem();
  MoFEMErrorCode setupAssemblySystem();
  MoFEMErrorCode setupPostProcess();
  MoFEMErrorCode executeSetups();

  MoFEM::Interface &mField;
  Simple *simpleInterface;

  SmartPetscObj<DM> dM;
  SmartPetscObj<TS> tS;

  DataValues dataValues;

  std::map<std::string, BlockData> setOfBlocks;

  Range intialValueEnts;
  Range essentialBoundEnts;

  int oRder;

  boost::shared_ptr<FaceEle> facePipelineNonStiffRhs;
  boost::shared_ptr<FaceEle> facePipelineStiffRhs;
  boost::shared_ptr<FaceEle> facePipelineStiffLhs;

  boost::shared_ptr<FaceEle> facePipelineInitial;

  // boost::shared_ptr<PostProcFaceOnRefinedMesh> postProc;
  boost::shared_ptr<PostProc> postProc;
  boost::shared_ptr<Monitor> monitorPtr;

  boost::shared_ptr<CommonData> commonData;

  boost::shared_ptr<ForcesAndSourcesCore> null;
}; // FEProblem

FEProblem::FEProblem(MoFEM::Interface &m_field)
    : mField(m_field), oRder(2) {
  facePipelineNonStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineStiffLhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineInitial = boost::shared_ptr<FaceEle>(new FaceEle(m_field));

  // postProc = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
  //     new PostProcFaceOnRefinedMesh(m_field));

  postProc = boost::shared_ptr<PostProc>(new PostProc(m_field));

  commonData = boost::shared_ptr<CommonData>(new CommonData());
}

MoFEMErrorCode FEProblem::setupInput() {
  MoFEMFunctionBegin;
  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  CHKERR simpleInterface->loadFile();
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::findBlocks() {
  MoFEMFunctionBegin;

  auto get_radial_cond = [](std::string block_name){
    double kr;
    if(block_name == "COAT"){
      kr = D_c;
    }else if(block_name == "WALL"){
      kr = Dm_r;
    } else{
      kr = K;
    }
    return Dm_z;
  };

  auto get_axial_cond = [](std::string block_name) {
    double kz;
    if (block_name == "COAT") {
      kz = D_c;
    } else if (block_name == "WALL") {
      kz = Dm_z;
    } else {
      kz = Dm_z;
    }
    return kz;
  };
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
    std::string name = it->getName();

    const int id = it->getMeshsetId();
    setOfBlocks[name].iD = id;
    if (name.compare(0, 4, "COAT") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, setOfBlocks[name].eNts, true);
      double kr = (timeScale / (lengthScale * lengthScale)) * get_radial_cond(name);
      double kz = (timeScale / (lengthScale * lengthScale)) * get_axial_cond(name);
      setOfBlocks[name].radialDiffuse = kr;
      setOfBlocks[name].axialDiffuse = kz;
    } if (name.compare(0, 4, "WALL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, setOfBlocks[name].eNts, true);
      double kr = (timeScale / (lengthScale * lengthScale)) * get_radial_cond(name);
      double kz = (timeScale / (lengthScale * lengthScale)) * get_axial_cond(name);
      setOfBlocks[name].radialDiffuse = kr;
      setOfBlocks[name].axialDiffuse = kz;
    } else if (name.compare(0, 7, "INITIAL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, setOfBlocks[name].eNts, true);
      std::vector<double> init_val;
      it->getAttributes(init_val);
      setOfBlocks[name].initVal = 1.0;
    } else if (name.compare(0, 9, "ESSENTIAL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 1, essentialBoundEnts, true);
    }
  }
  MoFEMFunctionReturn(0);
}
MoFEMErrorCode FEProblem::setupFields() {
  MoFEMFunctionBegin;

  CHKERR simpleInterface->addDomainField("FreeDrugFlux", HCURL,
                                         AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->addDomainField("FreeDrug", L2, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->addDomainField("BoundDrug", L2, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-order", &oRder, PETSC_NULL);

  CHKERR simpleInterface->setFieldOrder("FreeDrug", oRder);
  CHKERR simpleInterface->setFieldOrder("BoundDrug", oRder);
  CHKERR simpleInterface->setFieldOrder("FreeDrugFlux", oRder + 1);

  CHKERR simpleInterface->setUp();

  dM = simpleInterface->getDM();
  tS = createTS(mField.get_comm());
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::setupIntegrationRule() {
  MoFEMFunctionBegin;
  auto vol_rule = [](int, int, int p) -> int { return 2 * p; };

  facePipelineStiffRhs->getRuleHook = vol_rule;

  facePipelineStiffLhs->getRuleHook = vol_rule;
  facePipelineInitial->getRuleHook = vol_rule;
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::applyBIcondition() {
  MoFEMFunctionBegin;
  facePipelineInitial->getOpPtrVector().push_back(
      new OpInitialMass("FreeDrug", initVal, &setOfBlocks["INITIAL"].eNts));

  CHKERR DMoFEMLoopFiniteElements(dM, simpleInterface->getDomainFEName(),
                                  facePipelineInitial);

  CHKERR mField.getInterface<ProblemsManager>()->removeDofsOnEntities(
      "SimpleProblem", "FreeDrugFlux", essentialBoundEnts);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
FEProblem::updateElements(boost::shared_ptr<FaceEle> &fe_ele) {
  MoFEMFunctionBegin;
  fe_ele->getOpPtrVector().push_back(
      new OpCalculateJacForFace(commonData->jAc));
  fe_ele->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(commonData->invJac));

  fe_ele->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());

  fe_ele->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformFace(commonData->jAc));

  fe_ele->getOpPtrVector().push_back(
      new OpSetInvJacHcurlFace(commonData->invJac));

  MoFEMFunctionReturn(0);
}


MoFEMErrorCode FEProblem::setupFETSsystem() {
  MoFEMFunctionBegin;

  // Non-stiff Rhs
  CHKERR updateElements(facePipelineNonStiffRhs);
  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("FreeDrug", commonData->freeDrugValPtr));
  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("BoundDrug", commonData->boundDrugValPtr));

  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpRhsExplicit("FreeDrug", OpFreeDrugSrc(commonData), &setOfBlocks["WALL"].eNts));
  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpRhsExplicit("BoundDrug", OpBoundDrugSrc(commonData), &setOfBlocks["WALL"].eNts));

  // Stiff Rhs
  CHKERR updateElements(facePipelineStiffRhs);

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("FreeDrug", commonData->freeDrugValPtr));
  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorField<3>("FreeDrugFlux", commonData->freeDrugFluxPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarValuesDot("FreeDrug", commonData->freeDrugDotPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarValuesDot("BoundDrug", commonData->boundDrugDotPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorDivergence<3, 2>("FreeDrugFlux",
                                                commonData->freeDrugDivPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpRhsHDIV("FreeDrugFlux", commonData, setOfBlocks["COAT"],
                    &setOfBlocks["COAT"].eNts));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpRhsHDIV("FreeDrugFlux", commonData, setOfBlocks["WALL"],
                    &setOfBlocks["WALL"].eNts));

  facePipelineStiffRhs->getOpPtrVector().push_back(new OpRhsL2("FreeDrug", commonData));

  facePipelineStiffRhs->getOpPtrVector().push_back(new OpRhsL2Field2("BoundDrug", commonData));

  // Stiff Lhs
  CHKERR updateElements(facePipelineStiffLhs);

 
  PetscPrintf(PETSC_COMM_WORLD, "Kr : %1.3f \n", setOfBlocks["DOMAIN"].radialDiffuse);
  PetscPrintf(PETSC_COMM_WORLD, "Kz : %1.3f \n", setOfBlocks["DOMAIN"].axialDiffuse);

  facePipelineStiffLhs->getOpPtrVector().push_back(
    new OpLhsHDIV_HDIV("FreeDrugFlux", "FreeDrugFlux", commonData,
                         setOfBlocks["COAT"], &setOfBlocks["COAT"].eNts));

  facePipelineStiffLhs->getOpPtrVector().push_back(
    new OpLhsHDIV_HDIV("FreeDrugFlux", "FreeDrugFlux", commonData, setOfBlocks["WALL"],
      &setOfBlocks["WALL"].eNts));

  facePipelineStiffLhs->getOpPtrVector().push_back(new OpLhsHDIV_L2("FreeDrugFlux", "FreeDrug", commonData));
  facePipelineStiffLhs->getOpPtrVector().push_back(new OpLhsL2_HDIV("FreeDrug", "FreeDrugFlux", commonData));
  facePipelineStiffLhs->getOpPtrVector().push_back(new OpLhsL2_L2("FreeDrug", "FreeDrug", commonData));

  facePipelineStiffLhs->getOpPtrVector().push_back(new OpLhsL2_L2("BoundDrug", "BoundDrug", commonData));

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::setupAssemblySystem() {
  MoFEMFunctionBegin;
  CHKERR TSSetType(tS, TSARKIMEX);
  CHKERR TSARKIMEXSetType(tS, TSARKIMEXA2);

  CHKERR DMMoFEMTSSetRHSFunction(dM, simpleInterface->getDomainFEName(),
                                 facePipelineNonStiffRhs, null, null);

  CHKERR DMMoFEMTSSetIFunction(dM, simpleInterface->getDomainFEName(),
                               facePipelineStiffRhs, null, null);

  CHKERR DMMoFEMTSSetIJacobian(dM, simpleInterface->getDomainFEName(),
                               facePipelineStiffLhs, null, null);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::setupPostProcess() {
  MoFEMFunctionBegin;
  postProc->generateReferenceElementMesh();

  postProc->getOpPtrVector().push_back(
      new OpCalculateJacForFace(commonData->jAc));
  postProc->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(commonData->invJac));

  postProc->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());

  postProc->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformFace(commonData->jAc));

  postProc->getOpPtrVector().push_back(
      new OpSetInvJacHcurlFace(commonData->invJac));

  postProc->addFieldValuesPostProc("FreeDrugFlux");
  postProc->addFieldValuesPostProc("FreeDrug");
  postProc->addFieldValuesPostProc("BoundDrug");

  auto facePipelineIntegrateField = boost::shared_ptr<FaceEle>(new FaceEle(mField));

  CHKERR updateElements(facePipelineIntegrateField);

  facePipelineIntegrateField->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("FreeDrug", commonData->freeDrugValPtr));

  facePipelineIntegrateField->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("BoundDrug", commonData->boundDrugValPtr));

  boost::shared_ptr<VectorDouble> null_vector;

  auto calc_data_value = [&](auto operator_func, double &data_val, Range *entsPtr = NULL) {
    facePipelineIntegrateField->getOpPtrVector().push_back(
        new FeTools::OpIntegrateData("FreeDrug", data_val, operator_func, mField, entsPtr));
  };

  calc_data_value(OpVolume(commonData), dataValues.CoatingVolume, &setOfBlocks["COAT"].eNts);
  calc_data_value(OpVolume(commonData), dataValues.WallVolume, &setOfBlocks["WALL"].eNts);
  calc_data_value(OpFreeDrug(commonData), dataValues.FreeDrugCoating, &setOfBlocks["COAT"].eNts);
  calc_data_value(OpFreeDrug(commonData), dataValues.FreeDrugWall, &setOfBlocks["WALL"].eNts);
  calc_data_value(OpBoundDrug(commonData), dataValues.BoundDrugWall, &setOfBlocks["WALL"].eNts);
  calc_data_value(OpTotalDrug(commonData), dataValues.TotalDrugContent);
  calc_data_value(OpSquareDrug(commonData), dataValues.SquareDrugContent);


  monitorPtr = boost::shared_ptr<Monitor>(new Monitor(
      simpleInterface, facePipelineIntegrateField, postProc, mField, &dataValues));

  CHKERR DMMoFEMTSSetMonitor(dM, tS, simpleInterface->getDomainFEName(),
                             monitorPtr, null, null);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::executeSetups() {
  MoFEMFunctionBegin;

  TSAdapt adapt;

  // Create solution vector
  SmartPetscObj<Vec> X;
  CHKERR DMCreateGlobalVector_MoFEM(dM, X);
  CHKERR DMoFEMMeshToLocalVector(dM, X, INSERT_VALUES, SCATTER_FORWARD);
  // Solve FEproblem
  double ftime, dt;
  ftime = 1;
  CHKERR TSSetDM(tS, dM);
  CHKERR TSSetMaxTime(tS, ftime);
  CHKERR TSSetSolution(tS, X);
  // dt = 1e-5; /* Initial time step */
  // CHKERR TSSetTimeStep(tS, dt);

  // CHKERR TSGetAdapt(tS, &adapt);
  // CHKERR TSAdaptSetType(adapt, TSADAPTBASIC);
  // CHKERR TSAdaptSetStepLimits(
  //     adapt, 1e-12,
  //     1e-2); /* Also available with -ts_adapt_dt_min/-ts_adapt_dt_max */
  // CHKERR TSSetMaxSNESFailures(tS,
  //                             -1); /* Retry step an unlimited number of times */
  CHKERR TSSetFromOptions(tS);
  CHKERR TSSolve(tS, X);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::runAnalysis() {
  MoFEMFunctionBegin;
  CHKERR setupInput();
  CHKERR findBlocks();
  CHKERR setupFields();
  CHKERR setupIntegrationRule();
  CHKERR applyBIcondition();
  CHKERR setupFETSsystem();
  CHKERR setupAssemblySystem();
  CHKERR setupPostProcess();
  CHKERR executeSetups();
  MoFEMFunctionReturn(0);
}

int main(int argc, char *argv[]) {
  setenv("PYTHONPATH", ".", 1);
  CPyInstance hInstance;

  const char param_file[] = "param_file.petsc";

  MoFEM::Core::Initialize(&argc, &argv, param_file, help);
  try {
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);

    MoFEM::Interface &m_field = core;
    
    if (!data_fp)
      SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_SYS, "Cannot open file = %s \n",
               "data_file");
    PetscFPrintf(PETSC_COMM_SELF, data_fp, "time,CoatingVolume,WallVolume,FreeDrugCoating,FreeDrugWall,BoundDrugWall,TotalDrug,SquareDrug\n");
    FEProblem simple_heat_equation(m_field);
    simple_heat_equation.runAnalysis();
    if (data_fp) {
      fclose(data_fp);
      data_fp = NULL;
    }
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}
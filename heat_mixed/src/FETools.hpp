#ifndef __FETOOLS_HPP__
#define __FETOOLS_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>

namespace FeTools {

using FaceEle = MoFEM::FaceElementForcesAndSourcesCoreSwitch<
                FaceElementForcesAndSourcesCore::NO_HO_GEOMETRY |
                FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
                FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCoreSwitch<
                EdgeElementForcesAndSourcesCore::NO_HO_GEOMETRY |
                EdgeElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using OpFaceEle = FaceEle::UserDataOperator;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;



struct OpIntegrateData : OpFaceEle {
  typedef boost::function<void(VectorDouble &, MatrixDouble &)> VectorFunction;
  OpIntegrateData(std::string                     field_name, 
                  double                          &out_val,
                  VectorFunction                  vec_func, 
                  MoFEM::Interface                &m_field,
                  Range                           *ents = NULL)

    : OpFaceEle(field_name, OpFaceEle::OPROW), 
      outVal(out_val),
      vecFunc(vec_func),
      eNts(ents),
      mField(m_field),
      fieldName(field_name){}

  double &outVal;
  Range *eNts;
  VectorDouble integrandField;
  VectorFunction vecFunc;
  MoFEM::Interface &mField;
  std::string      fieldName;

      MoFEMErrorCode
      doWork(int side, EntityType type, EntData &row_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();

    auto field_ptr = mField.get_field_structure(fieldName);
    auto space = field_ptr->getSpace();

    bool is_L2_and_mbtri = space == L2 && type == MBTRI;
    bool is_H1_and_mbvertex = space == H1 && type == MBVERTEX; 

    if(!is_L2_and_mbtri && !is_H1_and_mbvertex)
      MoFEMFunctionReturnHot(0);

    if (!nb_row_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    const double vol = getMeasure();

    auto gp_coords = getCoordsAtGaussPts();
    vecFunc(integrandField, gp_coords);

    auto t_field_data = getFTensor0FromVec(integrandField);

    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      double area = t_field_data;
      outVal += t_field_data * vol * t_w;
      ++t_field_data;
      ++t_w;
    }

    MoFEMFunctionReturn(0);
  }
};

};

#endif
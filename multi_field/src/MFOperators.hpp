#ifndef __MFOPERATORS_HPP__
#define __MFOPERATORS_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>

namespace ReactionDiffusion {

using FaceEle = MoFEM::FaceElementForcesAndSourcesCoreSwitch<
    FaceElementForcesAndSourcesCore::NO_HO_GEOMETRY |
    FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
    FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCoreSwitch<
    EdgeElementForcesAndSourcesCore::NO_HO_GEOMETRY |
    EdgeElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using OpFaceEle = FaceEle::UserDataOperator;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

const double B = 0.0;
const double B_epsilon = 0.0;


int save_every_nth_step = 4;
// const int order = 3; ///< approximation order
const double init_value = 1.0;
const double essen_value = 0;
const double natu_value = 0;
// const int dim = 3;
FTensor::Index<'i', 3> i;

struct PreviousData {
  MatrixDouble flux_values;
  VectorDouble flux_divs;

  VectorDouble mass_dots;
  VectorDouble mass_values;
  MatrixDouble mass_grads;

  VectorDouble slow_values;
};

auto get_nb_dofs = [](EntData &data, Tag th_order, MoFEM::Interface &m_field) {
  if (data.getFieldDofs().empty())
    return 0;
  else {
    auto fe_ent = data.getFieldDofs()[0]->getEnt();
    int order;
    CHKERR m_field.get_moab().tag_get_data(th_order, &fe_ent, 1, &order);
    if (data.getFieldDofs()[0]->getSpace() == L2)
      return data.getFieldDofs()[0]->getOrderNbDofs(order - 1);
    else
      return data.getFieldDofs()[0]->getOrderNbDofs(order);
  }
};

struct BlockData {
  int block_id;
  double a11, a12, a13, a21, a22, a23, a31, a32, a33;

  MatrixDouble Aii;

  double r1, r2, r3;

  Range block_ents;

  double B0; // species mobility

  BlockData()
      : a11(1), a12(0), a13(0), a21(0), a22(1), a23(0), a31(0), a32(0), a33(1),
        B0(2e-3), r1(1), r2(1), r3(1) 
        {
          Aii.resize(3, 3, false);
          Aii.clear();

          Aii(0, 0) = a11;
          Aii(0, 1) = a12;
          Aii(0, 2) = a13;

          Aii(1, 0) = a21;
          Aii(1, 1) = a22;
          Aii(1, 2) = a23;

          Aii(2, 0) = a31;
          Aii(2, 1) = a32;
          Aii(2, 2) = a33;
        }
};





auto compute_rhs = [](int sp, std::vector<double> mass_values, BlockData & block_data) {

  int sz = mass_values.size();
  double res = 1.0;
  for(int ss = 0; ss < sz; ss++){
    res -= block_data.Aii(sp, ss) * mass_values[ss];
  }
  return res * mass_values[sp];
};

struct OpComputeSlowValue : public OpFaceEle {
  OpComputeSlowValue(std::string mass_field,
                     boost::shared_ptr<PreviousData> &data1,
                     boost::shared_ptr<PreviousData> &data2,
                     boost::shared_ptr<PreviousData> &data3,
                     std::map<int, BlockData> &block_map)
      : OpFaceEle(mass_field, OpFaceEle::OPROW), commonData1(data1),
        commonData2(data2), commonData3(data3), massField(mass_field),
        setOfBlock(block_map) {}
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    boost::shared_ptr<VectorDouble> slow_value_ptr1(commonData1,
                                                    &commonData1->slow_values);
    boost::shared_ptr<VectorDouble> slow_value_ptr2(commonData2,
                                                    &commonData2->slow_values);
    boost::shared_ptr<VectorDouble> slow_value_ptr3(commonData3,
                                                    &commonData3->slow_values);

    VectorDouble &vec1 = *slow_value_ptr1;
    VectorDouble &vec2 = *slow_value_ptr2;
    VectorDouble &vec3 = *slow_value_ptr3;
    const int nb_integration_pts = getGaussPts().size2();
    if (type == MBVERTEX) {
      vec1.resize(nb_integration_pts, false);
      vec2.resize(nb_integration_pts, false);
      vec3.resize(nb_integration_pts, false);
      vec1.clear();
      vec2.clear();
      vec3.clear();
    }
    const int nb_dofs = data.getIndices().size();

    if (nb_dofs) {
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");

      auto &block_data = *block_data_ptr;

      const int nb_integration_pts = getGaussPts().size2();

      auto t_slow_values1 = getFTensor0FromVec(vec1);
      auto t_slow_values2 = getFTensor0FromVec(vec2);
      auto t_slow_values3 = getFTensor0FromVec(vec3);

      auto t_mass_values1 = getFTensor0FromVec(commonData1->mass_values);
      auto t_mass_values2 = getFTensor0FromVec(commonData2->mass_values);
      auto t_mass_values3 = getFTensor0FromVec(commonData3->mass_values);
      // cout << "r1 : " << block_data.r1 << endl;
      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        t_slow_values1 =
            block_data.r1 * t_mass_values1 *
            (1.0 - block_data.a11 * t_mass_values1 -
             block_data.a12 * t_mass_values2 - block_data.a13 * t_mass_values3);
        t_slow_values2 =
            block_data.r2 * t_mass_values2 *
            (1.0 - block_data.a21 * t_mass_values1 -
             block_data.a22 * t_mass_values2 - block_data.a23 * t_mass_values3);

        t_slow_values3 =
            block_data.r3 * t_mass_values3 *
            (1.0 - block_data.a31 * t_mass_values1 -
             block_data.a32 * t_mass_values2 - block_data.a33 * t_mass_values3);
        ++t_slow_values1;
        ++t_slow_values2;
        ++t_slow_values3;

        ++t_mass_values1;
        ++t_mass_values2;
        ++t_mass_values3;
      }
    }
    MoFEMFunctionReturn(0);
  }

private:
  std::string massField;
  boost::shared_ptr<PreviousData> commonData1;
  boost::shared_ptr<PreviousData> commonData2;
  boost::shared_ptr<PreviousData> commonData3;
  std::map<int, BlockData> setOfBlock;
};

struct OpEssentialBC : public OpEdgeEle {
  OpEssentialBC(const std::string &flux_field, Range &essential_bd_ents)
      : OpEdgeEle(flux_field, OpEdgeEle::OPROW),
        essential_bd_ents(essential_bd_ents) {}

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nb_dofs = data.getIndices().size();
    if (nb_dofs) {
      EntityHandle fe_ent = getFEEntityHandle();
      bool is_essential =
          (essential_bd_ents.find(fe_ent) != essential_bd_ents.end());
      if (is_essential) {
        int nb_gauss_pts = getGaussPts().size2();
        int size2 = data.getN().size2();
        if (3 * nb_dofs != static_cast<int>(data.getN().size2()))
          SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
                  "wrong number of dofs");
        nN.resize(nb_dofs, nb_dofs, false);
        nF.resize(nb_dofs, false);
        nN.clear();
        nF.clear();

        auto t_row_tau = data.getFTensor1N<3>();

        auto dir = getDirection();
        double len = sqrt(dir[0] * dir[0] + dir[1] * dir[1] + dir[2] * dir[2]);

        FTensor::Tensor1<double, 3> t_normal(-dir[1] / len, dir[0] / len,
                                             dir[2] / len);

        auto t_w = getFTensor0IntegrationWeight();
        const double vol = getMeasure();

        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          const double a = t_w * vol;
          for (int rr = 0; rr != nb_dofs; rr++) {
            auto t_col_tau = data.getFTensor1N<3>(gg, 0);
            nF[rr] += a * essen_value * t_row_tau(i) * t_normal(i);
            for (int cc = 0; cc != nb_dofs; cc++) {
              nN(rr, cc) += a * (t_row_tau(i) * t_normal(i)) *
                            (t_col_tau(i) * t_normal(i));
              ++t_col_tau;
            }
            ++t_row_tau;
          }
          ++t_w;
        }

        cholesky_decompose(nN);
        cholesky_solve(nN, nF, ublas::lower());

        for (auto &dof : data.getFieldDofs()) {
          dof->getFieldData() = nF[dof->getEntDofIdx()];
        }
      }
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble nN;
  VectorDouble nF;
  Range &essential_bd_ents;
};

struct OpSkeletonSource : public OpEdgeEle {
  typedef boost::function<double(const double)> FVal;
  typedef boost::function<double(const double, const double, const double)>
      ExactFunVal;
  OpSkeletonSource(const std::string &mass_field, FVal skeleton_fun,
                   ExactFunVal smooth_fun, Range &internal_edge_ents)
      : OpEdgeEle(mass_field, OpEdgeEle::OPROW),
        internalEdges(internal_edge_ents), smoothFun(smooth_fun),
        skeletonFun(skeleton_fun) {}

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nb_dofs = data.getIndices().size();
    if (nb_dofs) {
      EntityHandle fe_ent = getFEEntityHandle();
      bool is_intEdge = (internalEdges.find(fe_ent) != internalEdges.end());
      if (is_intEdge) {
        int nb_gauss_pts = getGaussPts().size2();
        int size2 = data.getN().size2();
        if (3 * nb_dofs != static_cast<int>(data.getN().size2()))
          SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
                  "wrong number of dofs");
        vecF.resize(nb_dofs, false);

        vecF.clear();

        auto t_row_v_base = data.getFTensor0N();

        auto t_w = getFTensor0IntegrationWeight();
        const double vol = getMeasure();
        double dt;
        CHKERR TSGetTimeStep(getFEMethod()->ts, &dt);
        double ct = getFEMethod()->ts_t - dt;
        auto t_coords = getFTensor1CoordsAtGaussPts();

        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          const double a = t_w * vol;
          double trace_val = -2.0 * skeletonFun(t_coords(NX)) *
                             smoothFun(t_coords(NX), t_coords(NY), ct);
          for (int rr = 0; rr != nb_dofs; ++rr) {

            vecF[rr] += a * trace_val * t_row_v_base;
          }
          ++t_row_v_base;
        }
        ++t_w;
      }
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  FVal skeletonFun;
  ExactFunVal smoothFun;
  VectorDouble vecF;
  Range &internalEdges;

  FTensor::Number<0> NX;
  FTensor::Number<1> NY;
  FTensor::Number<2> NZ;
};
// struct OpInvMass : public OpFaceEle {
//   OpInvMass(const std::string &mass_field,
//             boost::shared_ptr<PreviousData> &common_data,
//             MoFEM::Interface &m_field)
//       : OpFaceEle(mass_field, OpFaceEle::OPROW), mField(m_field),
//         commonData(common_data) {}
//   MatrixDouble locMat;
//   MatrixDouble locInvMat;
//   MoFEM::Interface &mField;
//   boost::shared_ptr<PreviousData> commonData;

//   MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
//     MoFEMFunctionBegin;
//     int nn_dofs = data.getFieldData().size();
//     if (nn_dofs) {
//       EntityHandle fe_ent = getFEEntityHandle();

//       const int nb_dofs = get_nb_dofs(data, maxOrder, mField);

//       int nb_gauss_pts = getGaussPts().size2();
//       // if (nb_dofs != static_cast<int>(data.getN().size2()))
//       //   SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
//       //           "wrong number of dofs");
//       locMat.resize(nn_dofs, nn_dofs, false);
//       locInvMat.resize(nn_dofs, nn_dofs, false);
//       locMat.clear();
//       locInvMat.clear();

//       int ii = nb_dofs;
//       for (int ii = 0; ii < nn_dofs; ++ii) {
//         locInvMat(ii, ii) = 1.0;
//         if (ii <= nb_dofs)
//           locMat(ii, ii) = 1.0;
//       }

//       auto t_w = getFTensor0IntegrationWeight();
//       const double vol = getMeasure();

//       for (int gg = 0; gg < nb_gauss_pts; gg++) {
//         auto t_row_mass = data.getFTensor0N(gg, 0);
//         const double a = t_w * vol;
//         for (int rr = 0; rr != nb_dofs; rr++) {
//           auto t_col_mass = data.getFTensor0N(gg, 0);
//           for (int cc = 0; cc != nb_dofs; cc++) {
//             locMat(rr, cc) += a * t_row_mass * t_col_mass;
//             ++t_col_mass;
//           }
//           ++t_row_mass;
//         }
//         ++t_w;
//       }

//       cholesky_decompose(locMat);
//       cholesky_solve(locMat, locInvMat, ublas::lower());
//       cout << locInvMat << endl;

//       CHKERR MatSetValues(commonData->myS, data, data, &locInvMat(0, 0),
//                           ADD_VALUES);
//       // this is only to check
//       // data.getFieldData()[dof->getEntDofIdx()] = nF[dof->getEntDofIdx()];
//     }
//     MoFEMFunctionReturn(0);
//   }
// };
// Assembly of system mass matrix
// //***********************************************

// Mass matrix corresponding to the flux equation.
// 01. Note that it is an identity matrix

struct OpInitialMass : public OpFaceEle {
  OpInitialMass(const std::string &mass_field,
                Range &inner_surface, MoFEM::Interface &m_field)
      : OpFaceEle(mass_field, OpFaceEle::OPROW), innerSurface(inner_surface),
        mField(m_field), mass(mass_field) {}
  std::string mass;
  MatrixDouble nN;
  VectorDouble nF;
  Range &innerSurface;
  MoFEM::Interface &mField;

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nn_dofs = data.getFieldData().size();
    if (nn_dofs) {
      EntityHandle fe_ent = getFEEntityHandle();
      bool is_inner_side = (innerSurface.find(fe_ent) != innerSurface.end());
      if (is_inner_side) {
        const int nb_dofs = get_nb_dofs(data, maxOrder, mField);

        int nb_gauss_pts = getGaussPts().size2();
        // if (nb_dofs != static_cast<int>(data.getN().size2()))
        //   SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
        //           "wrong number of dofs");
        nN.resize(nn_dofs, nn_dofs, false);
        nF.resize(nn_dofs, false);
        nN.clear();
        nF.clear();

        int ii = nb_dofs;
        for (; ii < nn_dofs; ++ii)
          nN(ii, ii) = 1.0;

        auto t_w = getFTensor0IntegrationWeight();
        const double vol = getMeasure();

        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          auto t_row_mass = data.getFTensor0N(gg, 0);
          const double a = t_w * vol;
          // double r = ((double) rand() / (RAND_MAX));
          for (int rr = 0; rr != nb_dofs; rr++) {
            auto t_col_mass = data.getFTensor0N(gg, 0);
            nF[rr] += a * init_value * t_row_mass;
            for (int cc = 0; cc != nb_dofs; cc++) {
              nN(rr, cc) += a * t_row_mass * t_col_mass;
              ++t_col_mass;
            }
            ++t_row_mass;
          }
          ++t_w;
        }

        cholesky_decompose(nN);
        cholesky_solve(nN, nF, ublas::lower());
        // cout << "nF : " << nF << endl;

        for (auto &dof : data.getFieldDofs()) {
          dof->getFieldData() = nF[dof->getEntDofIdx()];

          // this is only to check
          // data.getFieldData()[dof->getEntDofIdx()] = nF[dof->getEntDofIdx()];
        }
      }
    }
    MoFEMFunctionReturn(0);
  }
};

// Assembly of RHS for explicit (slow)
// part//**************************************

// 2. RHS for explicit part of the mass balance equation
struct OpAssembleSlowRhsV : OpFaceEle // R_V
{
  typedef boost::function<double(const double, const double, const double)>
      FVal;
  typedef boost::function<FTensor::Tensor1<double, 3>(
      const double, const double, const double)>
      FGrad;
  OpAssembleSlowRhsV(std::string                                  mass_field,
                     std::vector<boost::shared_ptr<PreviousData>> &common_data,
                     int                                          ns,
                     std::map<int, BlockData>                     &block_map,
                     FVal                                         exact_value, 
                     FVal                                         exact_dot, 
                     FVal                                         exact_lap,
                     MoFEM::Interface                             &m_field)
      : OpFaceEle(mass_field, OpFaceEle::OPROW)
      , commonData(common_data)
      , nS(ns)
      , exactValue(exact_value)
      , exactDot(exact_dot)
      , exactLap(exact_lap)
      , mField(m_field) 
      , setOfBlock(block_map)
      {}
  int nS;
  std::map<int, BlockData>  setOfBlock;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nn_dofs = data.getIndices().size();
    if (nn_dofs) {
      const int nb_dofs = get_nb_dofs(data, maxOrder, mField);

      vecF.resize(nn_dofs, false);
      mat.resize(nn_dofs, nn_dofs, false);
      mat.clear();
      vecF.clear();

      int ii = nb_dofs;
      for (; ii < nn_dofs; ++ii)
        mat(ii, ii) = 1.0;

      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
o

      auto &block_data = *block_data_ptr;

      const int nb_integration_pts = getGaussPts().size2();
      int sp = getFEMethod()->ts_step % nS;

      std::vector<FTensor::Tensor0<FTensor::PackPtr<double *, 1>> > t_mass_values;

      for(int ss = 0; ss < nS; ss++){
        t_mass_values.push_back(getFTensor0FromVec(commonData[ss]->mass_values));
      }

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      const double ct = getFEMethod()->ts_t;
      auto t_coords = getFTensor1CoordsAtGaussPts();
      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        std::vector<double> mass_values(nS);
        for(int ss = 0; ss < nS; ss++){
          mass_values[ss] = t_mass_values[ss];
        }
        double f = compute_rhs(sp, mass_values, block_data);
        // double u_dot = exactDot(t_coords(NX), t_coords(NY), ct);
        // double u_lap = exactLap(t_coords(NX), t_coords(NY), ct);

        // double f = u_dot - u_lap;

        // double f = t_mass_value * (1.0 - t_mass_value);
        auto t_row_v_base = data.getFTensor0N(gg, 0);
        for (int rr = 0; rr != nb_dofs; ++rr) {
          auto t_col_v_base = data.getFTensor0N(gg, 0);
          vecF[rr] += a * f * t_row_v_base;
          for (int cc = 0; cc != nb_dofs; ++cc) {
            mat(rr, cc) += a * t_row_v_base * t_col_v_base;
            ++t_col_v_base;
          }
          ++t_row_v_base;
        }
        for(int ss = 0; ss < nS; ss++)
          ++t_mass_values[ss];
        ++t_w;
        ++t_coords;
      }
      cholesky_decompose(mat);
      cholesky_solve(mat, vecF, ublas::lower());

      // cout << vecF << endl;

      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  std::vector<boost::shared_ptr<PreviousData>>  commonData;

  
  VectorDouble      vecF;
  MatrixDouble      mat;


  FVal   exactValue;
  FVal   exactDot;
  FVal   exactLap;

  MoFEM::Interface   &mField;

  FTensor::Number<0> NX;
  FTensor::Number<1> NY;
  FTensor::Number<2> NZ;
};

// 5. RHS contribution of the natural boundary condition
struct OpAssembleNaturalBCRhsTau : OpEdgeEle // R_tau_2
{
  OpAssembleNaturalBCRhsTau(std::string flux_field, Range &natural_bd_ents)
      : OpEdgeEle(flux_field, OpEdgeEle::OPROW),
        natural_bd_ents(natural_bd_ents) {}

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nb_dofs = data.getIndices().size();

    if (nb_dofs) {
      EntityHandle row_side_ent = data.getFieldDofs()[0]->getEnt();

      bool is_natural =
          (natural_bd_ents.find(row_side_ent) != natural_bd_ents.end());
      if (is_natural) {
        // cerr << "In NaturalBCRhsTau..." << endl;
        vecF.resize(nb_dofs, false);
        vecF.clear();
        const int nb_integration_pts = getGaussPts().size2();
        auto t_tau_base = data.getFTensor1N<3>();

        auto dir = getDirection();
        FTensor::Tensor1<double, 3> t_normal(-dir[1], dir[0], dir[2]);

        auto t_w = getFTensor0IntegrationWeight();

        for (int gg = 0; gg != nb_integration_pts; ++gg) {
          const double a = t_w;
          for (int rr = 0; rr != nb_dofs; ++rr) {
            vecF[rr] += (t_tau_base(i) * t_normal(i) * natu_value) * a;
            ++t_tau_base;
          }
          ++t_w;
        }
        CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                            PETSC_TRUE);
        CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                            ADD_VALUES);
      }
    }
    MoFEMFunctionReturn(0);
  }

private:
  VectorDouble vecF;
  Range natural_bd_ents;
};

// Assembly of RHS for the implicit (stiff) part excluding the essential
// boundary //**********************************
// 3. Assembly of F_tau excluding the essential boundary condition
template <int dim>
struct OpAssembleStiffRhsTau : OpFaceEle //  F_tau_1
{
  OpAssembleStiffRhsTau(std::string                                  flux_field,
                        std::vector<boost::shared_ptr<PreviousData>> &data,
                        int                                          ns,
                        std::map<int, BlockData>                     &block_map,
                        MoFEM::Interface                             &m_field)
      : OpFaceEle(flux_field, OpFaceEle::OPROW), commonData(data),
        setOfBlock(block_map), mField(m_field), nS(ns) {}
 
  int nS;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;

    const int nn_dofs = data.getIndices().size();
    if (nn_dofs) {
      const int max_order = data.getFieldDofs()[0]->getMaxOrder();
      const int nb_dofs = get_nb_dofs(data, maxOrder, mField);
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      vecF.resize(nn_dofs, false);
      vecF.clear();
      int sp = getFEMethod()->ts_step % nS;
      const int nb_integration_pts = getGaussPts().size2();
      auto t_flux_value = getFTensor1FromMat<3>(commonData[sp]->flux_values);
      auto t_mass_value = getFTensor0FromVec(commonData[sp]->mass_values);

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg < nb_integration_pts; ++gg) {

        auto t_tau_base = data.getFTensor1N<3>(gg, 0);

        auto t_tau_grad = data.getFTensor2DiffN<3, 2>(gg, 0);

        const double K = B_epsilon + (block_data.B0 + B * t_mass_value);
        const double K_inv = 1. / K;
        const double a = vol * t_w;
        for (int rr = 0; rr < nb_dofs; ++rr) {
          double div_base = t_tau_grad(0, 0) + t_tau_grad(1, 1);
          vecF[rr] += (K_inv * t_tau_base(i) * t_flux_value(i) -
                       div_base * t_mass_value) *
                      a;
          ++t_tau_base;
          ++t_tau_grad;
        }
        ++t_flux_value;
        ++t_mass_value;
        ++t_w;
      }
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  std::vector<boost::shared_ptr<PreviousData>> commonData;
  VectorDouble vecF;
  std::map<int, BlockData> setOfBlock;
  MoFEM::Interface &mField;
};
// 4. Assembly of F_v
template <int dim>
struct OpAssembleStiffRhsV : OpFaceEle // F_V
{
  typedef boost::function<double(const double, const double, const double)>
      FVal;
  OpAssembleStiffRhsV(std::string flux_field,
                      std::vector<boost::shared_ptr<PreviousData>> &data,
                      int                                          ns,
                      std::map<int, BlockData> &block_map, FVal exact_value,
                      FVal exact_dot, FVal exact_lap, MoFEM::Interface &m_field)
      : OpFaceEle(flux_field, OpFaceEle::OPROW), commonData(data),
        setOfBlock(block_map), exactValue(exact_value), exactDot(exact_dot),
        exactLap(exact_lap), mField(m_field), nS(ns) {}
  int nS;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nn_dofs = data.getIndices().size();
    // cerr << "In StiffRhsV ..." << endl;
    if (nn_dofs) {
      const int nb_dofs = get_nb_dofs(data, maxOrder, mField);

      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      vecF.resize(nn_dofs, false);
      vecF.clear();
      const int nb_integration_pts = getGaussPts().size2();
      int nstep = getFEMethod()->ts_step;
      int sp = nstep % nS;
      auto t_mass_dot = getFTensor0FromVec(commonData[sp]->mass_dots);
      auto t_flux_div = getFTensor0FromVec(commonData[sp]->flux_divs);

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      const double ct = getFEMethod()->ts_t;
      auto t_coords = getFTensor1CoordsAtGaussPts();
      for (int gg = 0; gg < nb_integration_pts; ++gg) {

        auto t_row_v_base = data.getFTensor0N(gg, 0);
        const double a = vol * t_w;
        double u_dot = exactDot(t_coords(NX), t_coords(NY), ct);
        double u_lap = exactLap(t_coords(NX), t_coords(NY), ct);

        // cout << "u_lap : " << u_lap << endl;

        // cout << "B0 : " << block_data.B0 << endl;

        // double f = u_dot - block_data.B0 * u_lap;
        double f = 0.0;
        for (int rr = 0; rr < nb_dofs; ++rr) {
          vecF[rr] += (t_row_v_base * (t_mass_dot + t_flux_div - f)) * a;
          ++t_row_v_base;
        }
        ++t_mass_dot;
        ++t_flux_div;
        ++t_w;
        ++t_coords;
      }
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  std::vector<boost::shared_ptr<PreviousData>> commonData;
  VectorDouble vecF;
  std::map<int, BlockData> setOfBlock;

  FVal exactValue;
  FVal exactDot;
  FVal exactLap;
  MoFEM::Interface &mField;

  FTensor::Number<0> NX;
  FTensor::Number<1> NY;
};

// Tangent operator
// //**********************************************
// 7. Tangent assembly for F_tautau excluding the essential boundary condition
template <int dim>
struct OpAssembleLhsTauTau : OpFaceEle // A_TauTau_1
{
  OpAssembleLhsTauTau(std::string                                  flux_field,
                      std::vector<boost::shared_ptr<PreviousData>> &commonData,
                      std::map<int, BlockData>                     &block_map,
                      const int                                    &sz,
                      MoFEM::Interface                             &m_field)
      : OpFaceEle(flux_field, flux_field, OpFaceEle::OPROWCOL)
      , setOfBlock(block_map)
      , commonData(commonData)
      , mField(m_field)
      , vecSize(sz) 
      {
        sYmm = false;
      }
  int vecSize;
  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nn_row_dofs = row_data.getIndices().size();
    const int nn_col_dofs = col_data.getIndices().size();

    if (nn_row_dofs && nn_col_dofs) {
      const int nb_row_dofs =
          get_nb_dofs(row_data, maxOrder, mField);
      const int nb_col_dofs =
          get_nb_dofs(col_data, maxOrder, mField);
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      mat.resize(nn_row_dofs, nn_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      int ns = getFEMethod()->ts_step % vecSize;
      auto t_mass_value = getFTensor0FromVec(commonData[ns]->mass_values);

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        auto t_row_tau_base = row_data.getFTensor1N<3>(gg, 0);
        const double a = vol * t_w;
        const double K = B_epsilon + (block_data.B0 + B * t_mass_value);
        const double K_inv = 1. / K;
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_tau_base = col_data.getFTensor1N<3>(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            mat(rr, cc) += (K_inv * t_row_tau_base(i) * t_col_tau_base(i)) * a;
            ++t_col_tau_base;
          }
          ++t_row_tau_base;
        }
        ++t_mass_value;
        ++t_w;
      }
      const bool on_block_diag = row_side == col_side && row_type == col_type;
      if (on_block_diag) {
        int ii = nb_row_dofs;
        for (; ii < nn_row_dofs; ++ii)
          mat(ii, ii) = 1.0;
      }

      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
      // if (!on_block_diag) {
      //   transMat.resize(nb_col_dofs, nb_row_dofs, false);
      //   noalias(transMat) = trans(mat);
      //   CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
      //                       &transMat(0, 0), ADD_VALUES);
      // }
    }
    MoFEMFunctionReturn(0);
  }

private:
  std::vector<boost::shared_ptr<PreviousData>> commonData;
  MatrixDouble                                 mat, transMat;
  Range                                        essential_bd_ents;
  std::map<int, BlockData>                     setOfBlock;
  MoFEM::Interface                             &mField;
};

// 9. Assembly of tangent for F_tau_v excluding the essential bc
template <int dim>
struct OpAssembleLhsTauV : OpFaceEle // E_TauV
{
  OpAssembleLhsTauV(std::string                                  flux_field, 
                    std::string                                  mass_field,
                    std::vector<boost::shared_ptr<PreviousData>> &data,
                    std::map<int, BlockData>                     &block_map,
                    const int                                    &sz,
                    MoFEM::Interface                             &m_field)
      : OpFaceEle(flux_field, mass_field, OpFaceEle::OPROWCOL)
      , commonData(data)
      , setOfBlock(block_map)
      , mField(m_field)
      , vecSize(sz)
      {
        sYmm = false;
      }
  int vecSize;
  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nn_row_dofs = row_data.getIndices().size();
    const int nn_col_dofs = col_data.getIndices().size();

    if (nn_row_dofs && nn_col_dofs) {
      const int nb_row_dofs =
          get_nb_dofs(row_data, maxOrder, mField);
      const int nb_col_dofs =
          get_nb_dofs(col_data, maxOrder, mField);
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;
      mat.resize(nn_row_dofs, nn_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();
      int ns = getFEMethod()->ts_step % vecSize;
      auto t_mass_value = getFTensor0FromVec(commonData[ns]->mass_values);
      auto t_flux_value = getFTensor1FromMat<3>(commonData[ns]->flux_values);
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        auto t_row_tau_base = row_data.getFTensor1N<3>(gg, 0);

        auto t_row_tau_grad = row_data.getFTensor2DiffN<3, 2>(gg, 0);

        const double a = vol * t_w;
        const double K = B_epsilon + (block_data.B0 + B * t_mass_value);
        const double K_inv = 1. / K;
        const double K_diff = B;

        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_v_base = col_data.getFTensor0N(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            double div_row_base = t_row_tau_grad(0, 0) + t_row_tau_grad(1, 1);
            mat(rr, cc) += (-(t_row_tau_base(i) * t_flux_value(i) * K_inv *
                              K_inv * K_diff * t_col_v_base) -
                            (div_row_base * t_col_v_base)) *
                           a;
            ++t_col_v_base;
          }
          ++t_row_tau_base;
          ++t_row_tau_grad;
        }
        ++t_w;
        ++t_mass_value;
        ++t_flux_value;
      }

      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  std::vector<boost::shared_ptr<PreviousData>> commonData;
  MatrixDouble                                 mat;
  std::map<int, BlockData>                     setOfBlock;
  MoFEM::Interface                             &mField;
};

// 10. Assembly of tangent for F_v_tau
struct OpAssembleLhsVTau : OpFaceEle // C_VTau
{
  OpAssembleLhsVTau(std::string      mass_field, 
                    std::string      flux_field, 
                    MoFEM::Interface &m_field)
      : OpFaceEle(mass_field, flux_field, OpFaceEle::OPROWCOL)
      , mField(m_field)
      {
        sYmm = false;
      }

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nn_row_dofs = row_data.getIndices().size();
    const int nn_col_dofs = col_data.getIndices().size();

    if (nn_row_dofs && nn_col_dofs) {
      const int nb_row_dofs = get_nb_dofs(row_data, maxOrder, mField);
      const int nb_col_dofs = get_nb_dofs(col_data, maxOrder, mField);
      mat.resize(nn_row_dofs, nn_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {

        auto t_row_v_base = row_data.getFTensor0N(gg, 0);
        const double a = vol * t_w;
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_tau_grad = col_data.getFTensor2DiffN<3, 2>(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            double div_col_base = t_col_tau_grad(0, 0) + t_col_tau_grad(1, 1);
            mat(rr, cc) += (t_row_v_base * div_col_base) * a;
            ++t_col_tau_grad;
          }
          ++t_row_v_base;
        }
        ++t_w;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat;
  MoFEM::Interface &mField;
  Tag thOrder;
};

// 11. Assembly of tangent for F_v_v
struct OpAssembleLhsVV : OpFaceEle // D
{
  OpAssembleLhsVV(std::string mass_field,
                  MoFEM::Interface &m_field)
      : OpFaceEle(mass_field, mass_field, OpFaceEle::OPROWCOL), mField(m_field) {
    sYmm = false;
  }

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;

    const int nn_row_dofs = row_data.getIndices().size();
    const int nn_col_dofs = col_data.getIndices().size();
    if (nn_row_dofs && nn_col_dofs) {
      const int nb_row_dofs = get_nb_dofs(row_data, maxOrder, mField);
      const int nb_col_dofs = get_nb_dofs(col_data, maxOrder, mField);
      mat.resize(nn_row_dofs, nn_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();

      auto t_w = getFTensor0IntegrationWeight();
      const double ts_a = getFEMethod()->ts_a;
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        auto t_row_v_base = row_data.getFTensor0N(gg, 0);

        const double a = vol * t_w;

        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_v_base = col_data.getFTensor0N(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            mat(rr, cc) += (ts_a * t_row_v_base * t_col_v_base) * a;

            ++t_col_v_base;
          }
          ++t_row_v_base;
        }
        ++t_w;
      }
      const bool on_block_diag = row_side == col_side && row_type == col_type;
      if (on_block_diag) {
        int ii = nb_row_dofs;
        for (; ii < nn_row_dofs; ++ii)
          mat(ii, ii) = 1.0;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
      // if (!on_block_diag) {
      //   transMat.resize(nb_col_dofs, nb_row_dofs, false);
      //   noalias(transMat) = trans(mat);
      //   CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
      //                       &transMat(0, 0), ADD_VALUES);
      // }
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat, transMat;
  MoFEM::Interface &mField;
  Tag thOrder;
};

struct OpError : public OpFaceEle {
  typedef boost::function<double(const double, const double, const double)>
      FVal;
  typedef boost::function<FTensor::Tensor1<double, 3>(
      const double, const double, const double)>
      FGrad;
  double &eRror0;
  double &eRror1;
  OpError(FVal exact_value, FVal exact_lap, FGrad exact_grad,
          boost::shared_ptr<PreviousData> &prev_data,
          std::map<int, BlockData> &block_map, double &err0, double &err1,
          MoFEM::Interface &m_field)
      : OpFaceEle("ERROR", OpFaceEle::OPROW), exactVal(exact_value),
        exactLap(exact_lap), exactGrad(exact_grad), prevData(prev_data),
        setOfBlock(block_map), eRror0(err0), eRror1(err1), mField(m_field) {}
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nn_dofs = data.getFieldData().size();
    // cout << "nb_error_dofs : " << nb_dofs << endl;
    if (nn_dofs) {
      const int nb_dofs = get_nb_dofs(data, maxOrder, mField);
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");

      auto &block_data = *block_data_ptr;

      auto t_flux_value = getFTensor1FromMat<3>(prevData->flux_values);
      // auto t_mass_dot = getFTensor0FromVec(prevData->mass_dots);
      auto t_mass_value = getFTensor0FromVec(prevData->mass_values);
      auto t_flux_div = getFTensor0FromVec(prevData->flux_divs);
      data.getFieldData().clear();
      const double vol = getMeasure();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();
      double dt;
      CHKERR TSGetTimeStep(getFEMethod()->ts, &dt);
      double ct = getFEMethod()->ts_t;
      auto t_coords = getFTensor1CoordsAtGaussPts();

      FTensor::Tensor1<double, 3> t_exact_flux, t_flux_error;
      double error0 = 0;
      double error1 = 0;
      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;
        double mass_exact = exactVal(t_coords(NX), t_coords(NY), ct);
        double flux_lap =
            -block_data.B0 * exactLap(t_coords(NX), t_coords(NY), ct);
        t_exact_flux(i) =
            -block_data.B0 * exactGrad(t_coords(NX), t_coords(NY), ct)(i);
        t_flux_error(0) = t_flux_value(0) - t_exact_flux(0);
        t_flux_error(1) = t_flux_value(1) - t_exact_flux(1);
        t_flux_error(2) = 0.0;
        double local_error = pow(mass_exact - t_mass_value, 2) +
                             t_flux_error(i) * t_flux_error(i) +
                             pow(flux_lap - t_flux_div, 2);
        // cout << "flux_div : " << t_flux_div << "   flux_exact : " <<
        // flux_exact << endl;
        error0 += a * pow(mass_exact - t_mass_value, 2);
        error1 += a * (pow(flux_lap - t_flux_div, 2) +
                       t_flux_error(i) * t_flux_error(i));
        // eRror += error1 + error2;

        ++t_w;
        ++t_mass_value;
        ++t_flux_div;
        ++t_flux_value;
        // ++t_mass_dot;
        ++t_coords;
      }
      eRror0 += error0;
      eRror1 += error1;

      data.getFieldDofs()[0]->getFieldData() =
          sqrt(error0) + sqrt(error1); // data.getFieldData()[0];
    }
    MoFEMFunctionReturn(0);
  }

private:
  FVal exactVal;
  FVal exactLap;
  FGrad exactGrad;
  boost::shared_ptr<PreviousData> prevData;
  std::map<int, BlockData> setOfBlock;
  MoFEM::Interface &mField;

  FTensor::Number<0> NX;
  FTensor::Number<1> NY;
};

struct OpPostError : public OpFaceEle {
  typedef boost::function<double(const double, const double, const double)>
      FVal;

  OpPostError(std::string post_field_name, FVal exact_dot, FVal exact_div,
              boost::shared_ptr<PreviousData> &prev_data,
              std::map<int, BlockData> &block_map, double &cum_error,
              MoFEM::Interface &m_field)
      : OpFaceEle(post_field_name, OpFaceEle::OPROW), prevData(prev_data),
        setOfBlock(block_map), exactDot(exact_dot), exactDiv(exact_div),
        cumError(cum_error), mField(m_field) {}
  double &cumError;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nn_dofs = data.getFieldData().size();
    // cout << "nb_error_dofs : " << nb_dofs << endl;
    EntityHandle fe_ent = getFEEntityHandle();
    if (nn_dofs) {
      const int nb_dofs = get_nb_dofs(data, maxOrder, mField);
      auto find_block_data = [&]() {
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");

      auto &block_data = *block_data_ptr;

      auto t_flux_value = getFTensor1FromMat<3>(prevData->flux_values);
      auto t_mass_dot = getFTensor0FromVec(prevData->mass_dots);
      auto t_mass_grad = getFTensor1FromMat<2>(prevData->mass_grads);
      auto t_flux_div = getFTensor0FromVec(prevData->flux_divs);
      // data.getFieldData().clear();

      const double vol = getMeasure();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();

      double dt;
      CHKERR TSGetTimeStep(getFEMethod()->ts, &dt);
      double ct = getFEMethod()->ts_t;
      auto t_coords = getFTensor1CoordsAtGaussPts();

      // cout << "no. integration pts : " << nb_integration_pts << endl;

      // t_mass_grad(i) = -block_data.B0 * t_mass_grad(i);

      FTensor::Tensor1<double, 3> t_constitutive_error;
      double eta1 = 0.0;
      double eta2 = 0.0;
      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;
        double mass_exact_dot = exactDot(t_coords(NX), t_coords(NY), ct);
        double mass_exact_div = exactDiv(t_coords(NX), t_coords(NY), ct);

        // cout << "mass_grad  : " << t_mass_grad << endl;
        // cout << "flux_value : " << t_flux_value << endl;
        // cout << "=======================" << endl;

        double K = block_data.B0;

        double f = mass_exact_dot - block_data.B0 * mass_exact_div;

        t_constitutive_error(0) =
            t_flux_value(0) + block_data.B0 * t_mass_grad(0);
        t_constitutive_error(1) =
            t_flux_value(1) + block_data.B0 * t_mass_grad(1);
        t_constitutive_error(2) = 0.0;

        // eta1 += a * pow(t_mass_dot + t_flux_div - f, 2);
        eta2 += a * t_constitutive_error(i) * t_constitutive_error(i);
        ++t_w;
        ++t_flux_div;
        ++t_flux_value;
        ++t_mass_dot;
        ++t_mass_grad;
        ++t_coords;
      }

      data.getFieldDofs()[0]->getFieldData() += sqrt(eta1) + sqrt(eta2);
      cumError += eta1 + eta2;
    }
    MoFEMFunctionReturn(0);
  }

private:
  FVal exactLap;
  FVal exactDot;
  FVal exactDiv;
  boost::shared_ptr<PreviousData> prevData;
  std::map<int, BlockData> setOfBlock;
  MoFEM::Interface &mField;

  FTensor::Number<0> NX;
  FTensor::Number<1> NY;
};

template <int nbSpecies>
struct Monitor : public FEMethod {
  MoFEM::Interface &mField;
  Monitor(MPI_Comm                                          &comm, 
          const int                                         &rank, 
          SmartPetscObj<DM>                                 &dm,
          SmartPetscObj<TS>                                 &ts, 
          boost::shared_ptr<FaceEle>                        &domain_pipeline,
          boost::shared_ptr<PostProcFaceOnRefinedMeshFor2D> &post_proc,
          MoFEM::Interface &m_field)
      : cOmm(comm)
      , rAnk(rank)
      , dM(dm)
      , tS(ts)
      , postProc(post_proc)
      , domainPipeline(domain_pipeline)
      , mField(m_field){};
  MoFEMErrorCode preProcess() {
    MoFEMFunctionBegin;
    MoFEMFunctionReturn(0);
  }
  MoFEMErrorCode operator()() { return 0; }
  MoFEMErrorCode postProcess() {
    MoFEMFunctionBegin;
    for (int sp = 0; sp < nbSpecies; sp++) {
      if (ts_step % nbSpecies == sp) {
        // cout << "Hello" << endl;
        auto dofM = mField.get_dofs_by_name_begin("M");
        auto dof_mass = mField.get_dofs_by_name_begin(mass_names[sp]);
        for (; dofM != mField.get_dofs_by_name_end("M"); ++dofM, ++dof_mass)
          dof_mass->get()->getFieldData() = dofM->get()->getFieldData();

        auto dofF = mField.get_dofs_by_name_begin("F");
        auto dof_flux = mField.get_dofs_by_name_begin(flux_names[sp]);
        for (; dofF != mField.get_dofs_by_name_end("F"); ++dofF, ++dof_flux)
          dof_flux->get()->getFieldData() = dofF->get()->getFieldData();
      }
    }
    int nstep_mod = ts_step / nbSpecies;
    // cout << "nstep_mod : " << nstep_mod << endl;

    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-save_every_nth_step",
                              &save_every_nth_step, PETSC_NULL);
    if (nstep_mod % save_every_nth_step == 0) {

      CHKERR DMoFEMLoopFiniteElements(dM, "dFE", postProc);
      CHKERR postProc->writeFile(
          "out_level_s" + boost::lexical_cast<std::string>(nstep_mod) + ".h5m");
    }

    CHKERR DMoFEMLoopFiniteElements(dM, "dFE", domainPipeline);
    auto gather_errors = [&](double &global_error, Vec vector_proc_error) {
      MoFEMFunctionBegin;
      global_error = 0;
      CHKERR VecCreateMPI(cOmm, 1, PETSC_DECIDE, &vector_proc_error);
      CHKERR VecSetValue(vector_proc_error, rAnk, global_error, INSERT_VALUES);

      CHKERR VecAssemblyBegin(vector_proc_error);
      CHKERR VecAssemblyEnd(vector_proc_error);

      MoFEMFunctionReturn(0);
    };

    double error_sum0 = 0;
    double error_sum1 = 0;
    double post_error_sum = 0;
    double error_max = 0;

    // Vec error_per_proc0;
    // Vec error_per_proc1;
    Vec post_error_proc;
    Vec vector_error_max_proc;

    // CHKERR gather_errors(eRror0, error_per_proc0);
    // CHKERR gather_errors(eRror1, error_per_proc1);
    // CHKERR gather_errors(pError, post_error_proc);
    // CHKERR gather_errors(maxError, vector_error_max_proc);

    // CHKERR VecCreateMPI(cOmm, 1, PETSC_DECIDE, &error_per_proc0);
    // CHKERR VecCreateMPI(cOmm, 1, PETSC_DECIDE, &error_per_proc1);
    CHKERR VecCreateMPI(cOmm, 1, PETSC_DECIDE, &post_error_proc);
    CHKERR VecCreateMPI(cOmm, 1, PETSC_DECIDE, &vector_error_max_proc);

    auto get_global_error = [&]() {
      MoFEMFunctionBegin;
      // CHKERR VecSetValue(error_per_proc0, rAnk, eRror0, INSERT_VALUES);
      // CHKERR VecSetValue(error_per_proc1, rAnk, eRror1, INSERT_VALUES);
      CHKERR VecSetValue(post_error_proc, rAnk, post_error, INSERT_VALUES);
      CHKERR VecSetValue(vector_error_max_proc, rAnk, maxError, INSERT_VALUES);
      MoFEMFunctionReturn(0);
    };
    CHKERR get_global_error();
    // CHKERR VecAssemblyBegin(error_per_proc0);
    // CHKERR VecAssemblyEnd(error_per_proc0);

    // CHKERR VecAssemblyBegin(error_per_proc1);
    // CHKERR VecAssemblyEnd(error_per_proc1);

    CHKERR VecAssemblyBegin(post_error_proc);
    CHKERR VecAssemblyEnd(post_error_proc);

    CHKERR VecAssemblyBegin(vector_error_max_proc);
    CHKERR VecAssemblyEnd(vector_error_max_proc);

    // CHKERR VecSum(error_per_proc0, &error_sum0);
    // CHKERR VecSum(error_per_proc1, &error_sum1);
    CHKERR VecSum(post_error_proc, &post_error_sum);
    CHKERR VecMax(vector_error_max_proc, PETSC_NULL, &error_max);

    // double exactError = sqrt(error_sum0) + sqrt(error_sum1);
    double postError = sqrt(post_error_sum);
    // CHKERR PetscPrintf(PETSC_COMM_WORLD, "True error      : %3.10e \n",
    // exactError); CHKERR PetscPrintf(PETSC_COMM_WORLD, "Post error      :
    // %3.10e \n", postError); CHKERR PetscPrintf(PETSC_COMM_WORLD, "Max local
    // error : %3.10e \n", error_max);
    auto poly_adapt = [&]() {
      // setting order of a face
      for (_IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(mField, "ERROR2", dof)) {
        double hi_theta = 1;
        double lo_theta = 0;
        CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-hi_theta", &hi_theta,
                                   PETSC_NULL);
        CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-lo_theta", &lo_theta,
                                   PETSC_NULL);

        double eta_k = dof->get()->getFieldData();

        auto face_ent = dof->get()->getEnt();

        bool lo_bound = (eta_k < error_max * lo_theta) ? true : false;
        bool hi_bound = (eta_k > error_max * hi_theta) ? true : false;

        int pOrder;
        CHKERR mField.get_moab().tag_get_data(thOrder, &face_ent, 1, &pOrder);

        if (lo_bound)
          pOrder = std::max(2, pOrder - 1);
        else if (hi_bound)
          pOrder = std::min(g_max_order, pOrder + 1);

        CHKERR mField.get_moab().tag_set_data(thOrder, &face_ent, 1, &pOrder);

        dof->get()->getFieldData() = 0.0;
      }

      for (_IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(mField, "ERROR", dof)) {
        auto face_ent = dof->get()->getEnt();
        int face_order;
        CHKERR mField.get_moab().tag_get_data(thOrder, &face_ent, 1,
                                              &face_order);
        dof->get()->getFieldData() = (double)face_order;
      }

      Range all_edge_entities;
      Range all_face_entities;
      CHKERR mField.get_moab().get_entities_by_type(0, MBEDGE,
                                                    all_edge_entities, false);
      CHKERR mField.get_moab().get_entities_by_type(0, MBTRI, all_face_entities,
                                                    false);

      // order an edge must be max of adjacent face orders
      for (auto edge : all_edge_entities) {
        int edge_order = 1;
        Range adj_faces;
        CHKERR mField.get_moab().get_adjacencies(&edge, 1, 2, false, adj_faces,
                                                 moab::Interface::UNION);
        // CHKERR PetscPrintf(PETSC_COMM_WORLD, "size of adj_faces : %2d \n",
        // adj_faces.size());
        for (auto face : adj_faces) {
          int face_order;
          CHKERR mField.get_moab().tag_get_data(thOrder, &face, 1, &face_order);
          edge_order = (edge_order > face_order) ? edge_order : face_order;
        }
        CHKERR mField.get_moab().tag_set_data(thOrder, &edge, 1, &edge_order);
      }
      ParallelComm *pcomm =
          ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);

      CHKERR pcomm->reduce_tags(thOrder, MPI_MAX, all_edge_entities);

      // set integration rule for face max order of the three adjacent edges
      for (auto face : all_face_entities) {
        int face_rule;
        CHKERR mField.get_moab().tag_get_data(thOrder, &face, 1, &face_rule);
        Range adj_edges;
        CHKERR mField.get_moab().get_adjacencies(&face, 1, 1, false, adj_edges,
                                                 moab::Interface::UNION);
        // cout << "size of adj_edge : " << adj_edges.size() << endl;

        for (auto edge : adj_edges) {
          int edge_order;
          CHKERR mField.get_moab().tag_get_data(thOrder, &edge, 1, &edge_order);

          face_rule = (face_rule > edge_order) ? face_rule : edge_order;
        }
        CHKERR mField.get_moab().tag_set_data(thRule, &face, 1, &face_rule);

        // cout << "face_rule : " << face_rule << endl;
      }
    };
    double tol = 1e-6;
    double rel_error = 1;
    // // if(ts_step % 2 == 0)
    // //   eta_0 = postError;
    // // else
    // //   eta_1 = postError;

    // // if(ts_step == 0){
    // //   rel_error = 1.;
    // //   eta_0 = 2.0;
    // //   eta_1 = 1.0;
    // // }
    global_error1 = postError;

    // CHKERR PetscPrintf(PETSC_COMM_WORLD, "eta1   : %3.10e \n", eta_1);
    if (ts_step > 0)
      rel_error = abs(global_error1 - global_error0) / global_error0;
    // CHKERR PetscPrintf(PETSC_COMM_WORLD, "eRror0    : %3.10e \n", eRror0);
    // CHKERR PetscPrintf(PETSC_COMM_WORLD, "eRror1    : %3.10e \n", eRror1);
    // CHKERR PetscPrintf(PETSC_COMM_WORLD, "Rel Err   : %3.10e \n", rel_error);

    if (postError > tol && ts_step >= 0) {
      // CHKERR PetscPrintf(PETSC_COMM_WORLD, "Refining... %1.1e \n", 0.0);
      poly_adapt();
      global_error0 = global_error1;
    }

    post_error = 0.0;
    maxError = 0.0;

    // for(auto edge : all_edge_entities){
    //   int edge_order;
    //   CHKERR mField.get_moab().tag_get_data(thOrder, &edge, 1, &edge_order);
    //   cout << "edge_order : " << edge_order << endl;
    // }

    MoFEMFunctionReturn(0);
  }

private:
  SmartPetscObj<DM> dM;
  SmartPetscObj<TS> tS;
  boost::shared_ptr<PostProcFaceOnRefinedMeshFor2D> postProc;
  boost::shared_ptr<FaceEle> domainPipeline;
  MPI_Comm cOmm;
  const int rAnk;
  Tag thOrder;
  Tag thRule;
};

}; // namespace ReactionDiffusion

#endif //__RDOPERATORS_HPP__




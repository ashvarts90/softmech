#include <stdlib.h>
#include <BasicFiniteElements.hpp>
#include <hexOperators.hpp>

using namespace MoFEM;
using namespace HexOps;

static char help[] = "...\n\n";

struct ShapeFunction {
public:
  ShapeFunction(moab::Core &mb_instance, MoFEM::Core &core);

  MoFEMErrorCode runAnalysis();

private:
  MoFEMErrorCode readMesh();
  MoFEMErrorCode setupSystem();
  MoFEMErrorCode assembleSystem();

  MoFEMErrorCode setOutput();

  boost::shared_ptr<VolumeEle> hexPipeline;

  boost::shared_ptr<VolumeEle> hexPostproc;


  MoFEM::Interface &mField;
  moab::Interface &moab;
  Simple *simpleInterface;

  DM dm;


  MPI_Comm mpiComm;  // mpi parallel communicator
  const int mpiRank; // number of processors involved

  std::string funcName;
};

ShapeFunction::ShapeFunction(moab::Core &mb_instance, MoFEM::Core &core)
    : funcName("N_pq")
    , moab(mb_instance), mField(core)
    , mpiComm(mField.get_comm())
    , mpiRank(mField.get_comm_rank()) 
    {
      hexPipeline = boost::shared_ptr<VolumeEle>(new VolumeEle(mField));
    }

MoFEMErrorCode ShapeFunction::readMesh() {
  MoFEMFunctionBegin;

  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  CHKERR simpleInterface->loadFile();

  MoFEMFunctionReturn(0);
}


MoFEMErrorCode ShapeFunction::setupSystem() {
  MoFEMFunctionBegin;

  CHKERR simpleInterface->addDomainField(funcName, H1, AINSWORTH_LEGENDRE_BASE, 3);
  CHKERR simpleInterface->setFieldOrder(funcName, 1);
  CHKERR simpleInterface->setUp();

  MoFEMFunctionReturn(0);
}


MoFEMErrorCode ShapeFunction::assembleSystem() {
  MoFEMFunctionBegin;
  hexPipeline->getOpPtrVector().push_back(new OpShapeFunction(funcName));

  // dm = simpleInterface->getDM();
  CHKERR simpleInterface->getDM(&dm);

  CHKERR DMoFEMLoopFiniteElements(dm, simpleInterface->getDomainFEName(), hexPipeline);
  MoFEMFunctionReturn(0);
}


MoFEMErrorCode ShapeFunction::setOutput() {
  MoFEMFunctionBegin;
  // CHKERR DMoFEMMeshToGlobalVector(dm, global_solution, INSERT_VALUES, SCATTER_REVERSE);
  
  hexPostproc =
      boost::shared_ptr<VolumeEle>(new PostProcVolumeOnRefinedMesh(mField));

  CHKERR
  boost::static_pointer_cast<PostProcVolumeOnRefinedMesh>(hexPostproc)
      ->generateReferenceElementMesh();
  CHKERR
  boost::static_pointer_cast<PostProcVolumeOnRefinedMesh>(hexPostproc)
      ->addFieldValuesPostProc(funcName);

  CHKERR DMoFEMLoopFiniteElements(dm, simpleInterface->getDomainFEName(),
                                  hexPostproc);

  CHKERR
  boost::static_pointer_cast<PostProcVolumeOnRefinedMesh>(hexPostproc)
      ->writeFile("shape_function.h5m");

  CHKERR DMDestroy(&dm);

  MoFEMFunctionReturn(0);
}



MoFEMErrorCode ShapeFunction::runAnalysis() {
  MoFEMFunctionBegin;

  CHKERR readMesh();
  CHKERR setupSystem();
  CHKERR assembleSystem();
  CHKERR setOutput();

  MoFEMFunctionReturn(0);
}

int main(int argc, char *argv[]) {
  const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, param_file, help);
  try {
    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);
    

    ShapeFunction shape_function(mb_instance, core);

    CHKERR shape_function.runAnalysis();
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}
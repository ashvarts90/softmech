#ifndef __OPCONTCHECK_HPP__
#define __OPCONTCHECK_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>

namespace Example {

using FaceEle = MoFEM::FaceElementForcesAndSourcesCoreSwitch<
                FaceElementForcesAndSourcesCore::NO_HO_GEOMETRY |
                FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
                FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using FaceEleOnSide = MoFEM::FaceElementForcesAndSourcesCoreOnSideSwitch<
                      FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
                      FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCoreSwitch<
                EdgeElementForcesAndSourcesCore::NO_HO_GEOMETRY |
                EdgeElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using OpFaceEle = FaceEle::UserDataOperator;
using OpFaceEleOnSide = FaceEleOnSide::UserDataOperator;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

struct SideData {
  MatrixDouble leftSideNormalFlux;
  MatrixDouble rightSideNormalFlux;

  VectorInt leftLocIndicies;
  VectorInt rightLocIndicies;

  EntityHandle leftEntity;
  EntityHandle rightEntity;
};

template <int DIM> struct OpAssembleStiffLhs : OpEle {

  OpAssembleStiffLhs(std::string fieldu, boost::shared_ptr<PreviousData> &data,
                     std::map<int, BlockData> &block_map)
      : OpEle(fieldu, fieldu, OpEle::OPROWCOL), commonData(data),
        setOfBlock(block_map) {
    sYmm = true;
  }
  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();
    // cerr << "In doWork() : (row, col) = (" << nb_row_dofs << ", " <<
    // nb_col_dofs << ")" << endl;
    if (nb_row_dofs && nb_col_dofs) {
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");

      auto &block_data = *block_data_ptr;

      mat.resize(nb_row_dofs, nb_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_row_base = row_data.getFTensor0N();

      auto t_val = getFTensor0FromVec(commonData->values);
      auto t_grad = getFTensor1FromMat<DIM>(commonData->grads);

      auto t_row_diff_base = row_data.getFTensor1DiffN<DIM>();
      auto t_w = getFTensor0IntegrationWeight();
      const double ts_a = getFEMethod()->ts_a;
      const double vol = getMeasure();

      FTensor::Index<'i', DIM> i;

      // cout << "B0 : " << block_data.B0 << endl;

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_base = col_data.getFTensor0N(gg, 0);
          auto t_col_diff_base = col_data.getFTensor1DiffN<DIM>(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {

            mat(rr, cc) +=
                a * (t_row_base * t_col_base * ts_a +
                     (block_data.B0 + B * t_val) * t_row_diff_base(i) *
                         t_col_diff_base(i) +
                     B * t_col_base * t_grad(i) * t_row_diff_base(i));

            ++t_col_base;
            ++t_col_diff_base;
          }
          ++t_row_base;
          ++t_row_diff_base;
        }
        ++t_w;
        ++t_val;
        ++t_grad;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
      if (row_side != col_side || row_type != col_type) {
        transMat.resize(nb_col_dofs, nb_row_dofs, false);
        noalias(transMat) = trans(mat);
        CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
                            &transMat(0, 0), ADD_VALUES);
      }
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  std::map<int, BlockData> setOfBlock;
  MatrixDouble mat, transMat;
};
struct SkeletonFE : public OpEdgeEle {

  template<int SpaceDim>  
  struct OpFillSideValues : public OpFaceEleOnSide {

    SideData &sideData;

    OpFillSideValues(std::string domain_field_name,
                    SideData    &side_data)
        : OpFaceEleOnSide(domain_field_name, OpFaceEleOnSide::OPCOL)
        , sideData(side_data) {}

    MoFEMErrorCode doWork(int        col_side, 
                          EntityType col_type,
                          EntData    &col_data) {
      MoFEMFunctionBeginHot;

      if (col_type == MBEDGE) {
        const int nb_integration_pts = getGaussPts().size2();
        const int nb_col_dofs = col_data.getIndices().size();

        auto &dir = getDirection();
        double len = sqrt(pow(dir(0), 2) + pow(dir(1), 2));
        FTensor::Tensor1<double, 2> t_normal{-dir(1)/len, dir(0)/len};

        auto t_col_diff = col_data.getFTensor1DiffN<SpaceDim>();

        FTensor::Index<'i', 2> i;

        MatrixDouble *ptr_normal_flux_data = nullptr;

        if (getFEMethod()->nInTheLoop == 0){
          ptr_normal_flux_data = &sideData.leftSideNormalFlux;
          sideData.leftEntity = getFEEntityHandle();
          sideData.leftLocIndicies = col_data.getIndices();
        } else {
          ptr_normal_flux_data = &sideData.rightSideNormalFlux;
          sideData.rightEntity = getFEEntityHandle();
          sideData.rightLocIndicies = col_data.getIndices();
        }
        MatrixDouble &normal_flux_data = *ptr_normal_flux_data;

        normal_flux_data.resize(nb_integration_pts, nb_col_dofs, false);

        for (size_t gg = 0; gg != nb_integration_pts; ++gg) {
          for (size_t bb = 0; bb != nb_col_dofs; ++bb) {
            normal_flux_data(gg, bb) = t_normal(i) * t_col_diff(i);
            ++t_col_diff;
          }
        }
      }
      MoFEMFunctionReturnHot(0);
    }
  };



  template<int SpaceDim>
  struct OpTangent : public OpFaceEleOnSide {

  SideData &sideData;
  MatrixDouble mat;

  OpTangent(std::string   domain_field_name,
            SideData      &side_data)
    : OpFaceEleOnSide(domain_field_name, OpFaceEleOnSide::OPROWCOL)
    , sideData(side_data){}

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data){
    MoFEMFunctionBegin;
    if (row_type == MBTRI && col_type == MBTRI) {
      const int nb_integration_pts = getGaussPts().size2();
      const int nb_row_dofs = row_data.getIndices().size();
      const int nb_col_dofs = col_data.getIndices().size();

      // sideData.leftSideNormalFlux.resize(nb_integration_pts, 0, false);
      // sideData.rightSideNormalFlux.resize(nb_integration_pts, 0, false);

      EntityHandle ent_handle = getFEEntityHandle();

      mat.resize(nb_row_dofs, nb_col_dofs, false);
      mat.clear();
      
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      auto calculate_tangent = [&](MatrixDouble &normal_flux) {
        auto t_row_base = row_data.getFTensor0N();
        for (size_t gg = 0; gg != nb_integration_pts; gg++) {
          double a = t_w * vol;
          for (size_t rr = 0; rr != nb_row_dofs; rr++) {
            for (size_t cc = 0; cc != nb_col_dofs; cc++) {
              mat(rr, cc) -= a * t_row_base * normal_flux(gg, cc);
            }
            ++t_row_base;
          }
          ++t_w;
        }
      };

      bool is_filled = sideData.leftSideNormalFlux.size2() != 0 &&
                        sideData.rightSideNormalFlux.size2() != 0;

      if (is_filled) {
        if (ent_handle == sideData.leftEntity) {
          col_data.getIndices().data().swap(sideData.rightLocIndicies.data());
          calculate_tangent(sideData.rightSideNormalFlux);

        } else if (ent_handle == sideData.rightEntity) {
          col_data.getIndices().data().swap(sideData.leftLocIndicies.data());
          calculate_tangent(sideData.leftSideNormalFlux);
        }
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
    } 
    MoFEMFunctionReturn(0);
  }
  };

  FaceEleOnSide normalTracesOnFaceSideFe;
  FaceEleOnSide tangetOnFaceSideFe;
  std::string   domainFieldName;
  SideData      &sideData;
  MatrixDouble  jac;
  MatrixDouble  invJac;
  

  SkeletonFE(std::string      skeleton_field_name,
             std::string      domain_field_name,
             MoFEM::Interface &m_field,
             SideData         &side_data)
    : OpEdgeEle(skeleton_field_name, OpEdgeEle::OPROW)
    , domainFieldName(domain_field_name)
    , sideData(side_data)
    , normalTracesOnFaceSideFe(m_field)
    , tangetOnFaceSideFe(m_field){

      normalTracesOnFaceSideFe.getOpPtrVector().push_back(
          new OpCalculateJacForFace(jac));

      normalTracesOnFaceSideFe.getOpPtrVector().push_back(
          new OpCalculateInvJacForFace(invJac));

      normalTracesOnFaceSideFe.getOpPtrVector().push_back(
          new OpSetInvJacH1ForFace(invJac));


      tangetOnFaceSideFe.getOpPtrVector().push_back(
          new OpCalculateJacForFace(jac));

      tangetOnFaceSideFe.getOpPtrVector().push_back(
          new OpCalculateInvJacForFace(invJac));

      tangetOnFaceSideFe.getOpPtrVector().push_back(
          new OpSetInvJacH1ForFace(invJac));

      normalTracesOnFaceSideFe.getOpPtrVector().push_back(
          new OpFillSideValues<2>(domainFieldName, sideData));
      tangetOnFaceSideFe.getOpPtrVector().push_back(
          new OpTangent<2>(domainFieldName, sideData));
    }
  
  MoFEMErrorCode doWork(int        side, 
                        EntityType type, 
                        EntData    &data) {
    MoFEMFunctionBeginHot;
    if (type == MBTRI){
      const size_t nb_integration_pts = data.getN().size1();

      sideData.leftSideNormalFlux.resize(nb_integration_pts, 0, false);
      sideData.rightSideNormalFlux.resize(nb_integration_pts, 0, false);

      CHKERR loopSideFaces("dFE", normalTracesOnFaceSideFe);

      CHKERR loopSideFaces("dFE", tangetOnFaceSideFe);
      
    }

    MoFEMFunctionReturnHot(0);
  }
};

};

#endif // __OPCONTCHECK_HPP__
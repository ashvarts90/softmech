#ifndef __MIXEDFE_HPP__
#define __MIXEDFE_HPP__

#include <stdlib.h>
#include <Python/Python.h>
#include "pyhelper.hpp"
#include <BasicFiniteElements.hpp>

using namespace MoFEM;
using namespace ErrorEstimates;
using namespace SimpleADDAnalyis;
using namespace FEOperators;

struct MixedFEProblem {
public:
  MixedFEProblem(MoFEM::Interface &m_field);

  MoFEMErrorCode runAnalysis();

private:
  MoFEMErrorCode setupInput();
  MoFEMErrorCode findBlocks();
  MoFEMErrorCode setupFields();
  MoFEMErrorCode setupIntegrationRule();
  MoFEMErrorCode applyBIcondition();
  MoFEMErrorCode updateElements(boost::shared_ptr<FaceEle> &fe_ele);
  MoFEMErrorCode setupFETSsystem();
  MoFEMErrorCode setupAssemblySystem();
  MoFEMErrorCode setupPostProcess();
  MoFEMErrorCode executeSetups();

  MoFEM::Interface &mField;
  Simple *simpleInterface;

  SmartPetscObj<DM> dM;
  SmartPetscObj<TS> tS;

  std::map<std::string, BlockData> setOfBlocks;

  Range essentialBdryEnts;

  Range naturalBdryEnts;

  Range intialValueEnts;

  int oRder;

  boost::shared_ptr<FaceEle> facePipelineNonStiffRhs;
  boost::shared_ptr<FaceEle> facePipelineStiffRhs;
  boost::shared_ptr<FaceEle> facePipelineStiffLhs;

  boost::shared_ptr<EdgeEle> edgePipelineNonStiffLhs;
  boost::shared_ptr<FaceEle> facePipelineInitial;

  boost::shared_ptr<EdgeEle> skeletonPipeline;

  boost::shared_ptr<PostProcFaceOnRefinedMesh> postProc;
  boost::shared_ptr<Monitor> monitorPtr;

  boost::shared_ptr<CommonData> commonData;

  boost::shared_ptr<ForcesAndSourcesCore> null;
};

MixedFEProblem::MixedFEProblem(MoFEM::Interface &m_field)
    : mField(m_field), oRder(2) {
  facePipelineNonStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  edgePipelineNonStiffLhs = boost::shared_ptr<EdgeEle>(new EdgeEle(m_field));
  facePipelineStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineStiffLhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineInitial = boost::shared_ptr<FaceEle>(new FaceEle(m_field));

  skeletonPipeline = boost::shared_ptr<EdgeEle>(new EdgeEle(m_field));

  postProc = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
      new PostProcFaceOnRefinedMesh(m_field));

  commonData = boost::shared_ptr<CommonData>(new CommonData());
}

MoFEMErrorCode MixedFEProblem::setupInput() {
  MoFEMFunctionBegin;
  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  CHKERR simpleInterface->loadFile();
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MixedFEProblem::findBlocks() {
  MoFEMFunctionBegin;

  CPyObject pName;
  CPyObject pModule;
  CPyObject pFunc_rad;
  CPyObject pFunc_axi;
  CPyObject pValue_rad;
  CPyObject pValue_axi;
  CPyObject pArgs;

  pName = PyUnicode_FromString((char *)"rhsFunc");
  pModule = PyImport_Import(pName);
  pFunc_rad = PyObject_GetAttrString(pModule, (char *)"radialDiffuse");
  pFunc_axi = PyObject_GetAttrString(pModule, (char *)"axialDiffuse");

  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
    std::string name = it->getName();

    char *name_charptr = const_cast<char *>(name.c_str());

    const int id = it->getMeshsetId();
    setOfBlocks[name].iD = id;
    if (name.compare(0, 6, "DOMAIN") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, setOfBlocks[name].eNts, true);
      pArgs = Py_BuildValue("(z)", name_charptr);
      pValue_rad = PyObject_CallObject(pFunc_rad, pArgs);
      pValue_axi = PyObject_CallObject(pFunc_axi, pArgs);
      setOfBlocks[name].radialDiffuse =
          (timeScale / (lengthScale * lengthScale)) *
          PyFloat_AsDouble(pValue_rad);
      setOfBlocks[name].axialDiffuse =
          (timeScale / (lengthScale * lengthScale)) *
          PyFloat_AsDouble(pValue_axi);
    } else if (name.compare(0, 7, "INITIAL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, setOfBlocks[name].eNts, true);
      std::vector<double> init_val;
      it->getAttributes(init_val);
      setOfBlocks[name].initVal = 1.0;
    } else if (name.compare(0, 9, "ESSENTIAL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 1, essentialBdryEnts, true);
    }
  }
  MoFEMFunctionReturn(0);
}
MoFEMErrorCode MixedFEProblem::setupFields() {
  MoFEMFunctionBegin;
  
  CHKERR simpleInterface->addDomainField("CTRN_FLUX", HCURL, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->addDomainField(     "CTRN", L2, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->addDomainField(      "AUX", L2, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->addDataField(         "CM", L2, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->addBoundaryField("CTRN_FLUX", HCURL, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-order", &oRder, PETSC_NULL);

  CHKERR simpleInterface->setFieldOrder("CTRN", oRder - 1);
  CHKERR simpleInterface->setFieldOrder("AUX", oRder - 1);
  CHKERR simpleInterface->setFieldOrder("CM", oRder - 1);
  CHKERR simpleInterface->setFieldOrder("CTRN_FLUX", oRder);

  CHKERR simpleInterface->setUp();

  dM = simpleInterface->getDM();
  tS = createTS(mField.get_comm());
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MixedFEProblem::setupIntegrationRule() {
  MoFEMFunctionBegin;
  auto vol_rule = [](int, int, int p) -> int { return 2 * p; };

  facePipelineNonStiffRhs->getRuleHook = vol_rule;
  edgePipelineNonStiffLhs->getRuleHook = vol_rule;

  facePipelineStiffRhs->getRuleHook = vol_rule;

  facePipelineStiffLhs->getRuleHook = vol_rule;
  facePipelineInitial->getRuleHook = vol_rule;
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MixedFEProblem::applyBIcondition() {
  MoFEMFunctionBegin;
  facePipelineInitial->getOpPtrVector().push_back(new OpLocMassMatrix(
      "CTRN", commonData->locMassMatPtr, &setOfBlocks["INITIAL"].eNts));

  facePipelineInitial->getOpPtrVector().push_back(new OpComputeFieldDofs(
      "CTRN", InitValAtGP(setOfBlocks["INITIAL"].initVal), AxisymmFun(1),
      commonData->locMassMatPtr, &setOfBlocks["INITIAL"].eNts));

  // facePipelineInitial->getOpPtrVector().push_back(
  //     new OpInitialMass("CTRN", setOfBlocks["INITIAL"].eNts,
  //                       setOfBlocks["INITIAL"].initVal, mField));

  CHKERR DMoFEMLoopFiniteElements(dM, simpleInterface->getDomainFEName(),
                                  facePipelineInitial);
  // cerr << "Essentail ents : " << essentialBdryEnts << endl;

  CHKERR mField.getInterface<ProblemsManager>()->removeDofsOnEntities(
      "SimpleProblem", "CTRN_FLUX", essentialBdryEnts);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
MixedFEProblem::updateElements(boost::shared_ptr<FaceEle> &fe_ele) {
  MoFEMFunctionBegin;
  fe_ele->getOpPtrVector().push_back(
      new OpCalculateJacForFace(commonData->jAc));
  fe_ele->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(commonData->invJac));
  fe_ele->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());
  if(axisymm){
  fe_ele->getOpPtrVector().push_back(
      new OpSetAxisymmPiolaTransformFace(commonData->jAc));
  } else {
    fe_ele->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformFace(commonData->jAc));
  }
  fe_ele->getOpPtrVector().push_back(
      new OpSetInvJacHcurlFace(commonData->invJac));

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MixedFEProblem::setupFETSsystem() {
  MoFEMFunctionBegin;

  // Non stiff Rhs
  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpCalculateJacForFace(commonData->jAc));
  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(commonData->invJac));
  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("CTRN", commonData->ctrnValPtr));
  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("AUX", commonData->auxValPtr));
  // facePipelineNonStiffRhs->getOpPtrVector().push_back(
  //     new OpCalculateScalarFieldValues("AUX", commonData->auxDotPtr));

  facePipelineNonStiffRhs->getOpPtrVector().push_back(new OpLocMassMatrix(
      "CTRN", commonData->locMassMatPtr, &setOfBlocks["DOMAIN_2"].eNts));

  facePipelineNonStiffRhs->getOpPtrVector().push_back(new OpRowFE<L2_0, EX>(
      "CTRN", FreeDrugSrc(commonData), AxisymmFun(1), commonData->locMassMatPtr,
      &setOfBlocks["DOMAIN_2"].eNts));

  // facePipelineNonStiffRhs->getOpPtrVector().push_back(new OpLocMassMatrix(
  //     "AUX", commonData->locMassMatPtr, &setOfBlocks["DOMAIN_2"].eNts));

  facePipelineNonStiffRhs->getOpPtrVector().push_back(new OpRowFE<L2_0, EX>(
      "AUX", BoundDrugSrc(commonData), AxisymmFun(1), commonData->locMassMatPtr,
      &setOfBlocks["DOMAIN_2"].eNts));

  // Stiff Rhs
  CHKERR updateElements(facePipelineStiffRhs);
  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCopyDofs("CTRN", "CM", &setOfBlocks["DOMAIN_2"].eNts));
  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("CTRN", commonData->ctrnValPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorField<3>("CTRN_FLUX", commonData->ctrnFluxPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarValuesDot("CTRN", commonData->ctrnDotPtr));
  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarValuesDot("CTRN", commonData->auxDotPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorDivergence<3, 2>("CTRN_FLUX",
                                                commonData->ctrnFluxDivPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(new OpRowFE<HDIV_0, IM>(
      "CTRN_FLUX", CtrnFluxResidual(commonData, setOfBlocks["DOMAIN_1"]),
      &setOfBlocks["DOMAIN_1"].eNts));
  facePipelineStiffRhs->getOpPtrVector().push_back(new OpRowFE<HDIV_0, IM>(
      "CTRN_FLUX", CtrnFluxResidual(commonData, setOfBlocks["DOMAIN_2"]),
      &setOfBlocks["DOMAIN_2"].eNts));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpRowFE<HDIV_1, IM>("CTRN_FLUX", CtrnFluxDivResidual(commonData)));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("AUX", commonData->auxDotPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpRowFE<L2_0, IM>("CTRN", CtrnResidual(commonData)));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpRowFE<L2_0, IM>("AUX", AuxDotResidual(commonData)));

  // Stiff Lhs
  CHKERR updateElements(facePipelineStiffLhs);

  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("CTRN", commonData->ctrnValPtr));
  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorField<3>("CTRN_FLUX", commonData->ctrnFluxPtr));

  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpRowColFE<HDIV_0, HDIV_0>("CTRN_FLUX", "CTRN_FLUX",
                                     Diffusivity(setOfBlocks["DOMAIN_1"]),
                                     &setOfBlocks["DOMAIN_1"].eNts));
  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpRowColFE<HDIV_0, HDIV_0>("CTRN_FLUX", "CTRN_FLUX",
                                     Diffusivity(setOfBlocks["DOMAIN_2"]),
                                     &setOfBlocks["DOMAIN_2"].eNts));

  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpRowColFE<L2_0, L2_0>("CTRN", "CTRN", CtrnTSshift()));

  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpRowColFE<L2_0, L2_0>("AUX", "AUX", CtrnTSshift()));

  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpRowColFE<HDIV_1, L2_0>("CTRN_FLUX", "CTRN", AxisymmFun(-1)));
  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpRowColFE<L2_0, HDIV_1>("CTRN", "CTRN_FLUX", AxisymmFun(1)));

  // Solve the axuliary variable
  // facePipelineStiffLhs->getOpPtrVector().push_back(
  //     new OpCalculateScalarFieldValues("AUX", commonData->auxValPtr));
  // // facePipelineStiffLhs->getOpPtrVector().push_back(
  // //     new OpCalculateScalarFieldValues("CTRN", commonData->ctrnValPtr));

  // facePipelineStiffLhs->getOpPtrVector().push_back(new OpComputeFieldAtGP(
  //     "AUX", StepUpAuxField(commonData), MBTRI, &setOfBlocks["DOMAIN_2"].eNts));

  // facePipelineStiffLhs->getOpPtrVector().push_back(new OpLocMassMatrix(
  //     "CTRN", commonData->locMassMatPtr, &setOfBlocks["DOMAIN_2"].eNts));

  // facePipelineStiffLhs->getOpPtrVector().push_back(new OpComputeFieldDofs(
  //     "AUX", AuxDof(commonData), AxisymmFun(1), commonData->locMassMatPtr,
  //     &setOfBlocks["DOMAIN_2"].eNts));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MixedFEProblem::setupAssemblySystem() {
  MoFEMFunctionBegin;
  CHKERR TSSetType(tS, TSARKIMEX);
  CHKERR TSARKIMEXSetType(tS, TSARKIMEXA2);

  CHKERR DMMoFEMTSSetIFunction(dM, simpleInterface->getDomainFEName(),
                               facePipelineStiffRhs, null, null);

  CHKERR DMMoFEMTSSetIJacobian(dM, simpleInterface->getDomainFEName(),
                               facePipelineStiffLhs, null, null);
  
  // CHKERR DMMoFEMTSSetRHSFunction(dM, simpleInterface->getDomainFEName(),
  //                                facePipelineNonStiffRhs, null, null);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MixedFEProblem::setupPostProcess() {
  MoFEMFunctionBegin;
  postProc->generateReferenceElementMesh();
  postProc->getOpPtrVector().push_back(
      new OpCalculateJacForFace(commonData->jAc));
  postProc->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(commonData->invJac));
  postProc->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());

  postProc->getOpPtrVector().push_back(
      new OpSetAxisymmPiolaTransformFace(commonData->jAc));

  postProc->getOpPtrVector().push_back(
      new OpSetInvJacHcurlFace(commonData->invJac));
  postProc->addFieldValuesPostProc("CTRN");
  // postProc->addFieldValuesPostProc("CTRN_FLUX");
  postProc->addFieldValuesPostProc("AUX");
  postProc->addFieldValuesPostProc("CM");

  monitorPtr = boost::shared_ptr<Monitor>(new Monitor(
      mField.get_comm(), mField.get_comm_rank(), dM, tS, postProc, mField));

  CHKERR DMMoFEMTSSetMonitor(dM, tS, simpleInterface->getDomainFEName(),
                             monitorPtr, null, null);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MixedFEProblem::executeSetups() {
  MoFEMFunctionBegin;

  TSAdapt adapt;

  // Create solution vector
  SmartPetscObj<Vec> X;
  CHKERR DMCreateGlobalVector_MoFEM(dM, X);
  CHKERR DMoFEMMeshToLocalVector(dM, X, INSERT_VALUES, SCATTER_FORWARD);
  // Solve problem
  double ftime, dt;
  ftime = 1;
  CHKERR TSSetDM(tS, dM);
  CHKERR TSSetMaxTime(tS, ftime);
  CHKERR TSSetSolution(tS, X);
  dt = 1e-5; /* Initial time step */
  CHKERR TSSetTimeStep(tS, dt);

  CHKERR TSGetAdapt(tS, &adapt);
  CHKERR TSAdaptSetType(adapt, TSADAPTBASIC);
  CHKERR TSAdaptSetStepLimits(
      adapt, 1e-12,
      1e-2); /* Also available with -ts_adapt_dt_min/-ts_adapt_dt_max */
  CHKERR TSSetMaxSNESFailures(tS,
                              -1); /* Retry step an unlimited number of times */
  CHKERR TSSetFromOptions(tS);
  CHKERR TSSolve(tS, X);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MixedFEProblem::runAnalysis() {
  MoFEMFunctionBegin;
  CHKERR setupInput();
  CHKERR findBlocks();
  CHKERR setupFields();
  CHKERR setupIntegrationRule();
  CHKERR applyBIcondition();
  CHKERR setupFETSsystem();
  CHKERR setupAssemblySystem();
  CHKERR setupPostProcess();
  CHKERR executeSetups();
  MoFEMFunctionReturn(0);
}

#endif //__MIXEDFE_HPP__
struct BlockData {
  int iD;
  double drugDiffusivity;
  double oN_ks;
  double oFf_ks;
  double mIn_bs;
  double mAx_bs;
  double initVal;

  int nFields;
  int dIm;
  std::vector<std::string> fieldNames;
  Range eNts;

  BlockData()
      : drugDiffusivity(-1), oN_ks(-1), oFf_ks(-1), mIn_bs(-1), mAx_bs(-1),
        nFields(-1), dIm(-1), initVal(0) {}
};

struct CommonData {
  MatrixDouble jAc;
  MatrixDouble invJac;

  MatrixDouble locMassMat;
  MatrixDouble locInvMassMat;

  boost::shared_ptr<VectorDouble> auxValPtr;

  boost::shared_ptr<VectorDouble> ctrnValPtr;
  boost::shared_ptr<VectorDouble> ctrnDotPtr;
  boost::shared_ptr<MatrixDouble> ctrnFluxPtr;
  boost::shared_ptr<VectorDouble> ctrnFluxDivPtr;

  std::map<int, BlockData> setOfBlocksData;

  CommonData() {
    jAc.resize(2, 2, false);
    invJac.resize(2, 2, false);

    auxValPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());

    ctrnValPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
    ctrnDotPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());

    ctrnFluxPtr = boost::shared_ptr<MatrixDouble>(new MatrixDouble());
    ctrnFluxDivPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
  }
};

struct ComputeAuxVal {
  double operator()(BlockData &block_data, const double ctrn_val_old,
                    const double aux_val_old, const double dt,
                    const double th) const {

    double on_ks = block_data.oN_ks;
    double off_ks = block_data.oFf_ks;
    double min_bs = block_data.mIn_bs;
    double max_bs = block_data.mAx_bs;

    double factor = (1.0 + dt * th * (on_ks * ctrn_val_old - off_ks));

    double numer = aux_val_old + dt * th * on_ks * ctrn_val_old * max_bs +
                   dt * (1 - th) *
                       (on_ks * ctrn_val_old * (max_bs - aux_val_old) -
                        off_ks * aux_val_old);
    return numer / factor;
  }
};

struct ComputeAuxDot {
  double operator()(BlockData &block_data, const double ctrn_val,
                    const double aux_val) const {
    double on_ks = block_data.oN_ks;
    double off_ks = block_data.oFf_ks;
    double min_bs = block_data.mIn_bs;
    double max_bs = block_data.mAx_bs;

    return 0.0;
    // return on_ks * ctrn_val * (max_bs - aux_val) - off_ks * aux_val;
  }
};
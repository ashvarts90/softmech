#include <stdlib.h>
#include <BasicFiniteElements.hpp>
#include <ChemoMechOperators.hpp>

using namespace MoFEM;
using namespace ChemoMechOps;

static char help[] = "...\n\n";

struct CoupledProblem {
public:
  CoupledProblem(moab::Core &mb_instance, MoFEM::Core &core);

  MoFEMErrorCode runAnalysis();

  int prob;

private:
  MoFEMErrorCode readMesh();
  MoFEMErrorCode setupSystem();
  MoFEMErrorCode setIntegrationRule();

  MoFEMErrorCode setupPipeline();
  MoFEMErrorCode setupIC();
  MoFEMErrorCode setupUpdateCommonData();
  MoFEMErrorCode setupBC();
  MoFEMErrorCode assembleSystem();
  MoFEMErrorCode solveSystem();
  MoFEMErrorCode setOutput();

  boost::shared_ptr<VolEle> volPipelineLHS;
  boost::shared_ptr<FaceEle> facePipelineLHS;

  boost::shared_ptr<VolEle> volPipelineNStiffRHS;

  boost::shared_ptr<VolEle> volPipelineStiffRHS;
  boost::shared_ptr<FaceEle> facePipelineStiffRHS;

  MoFEM::Interface &mField;
  Simple *simpleInterface;
  moab::Interface &moab;

  SmartPetscObj<DM> dM;
  SmartPetscObj<TS> tS;

  MPI_Comm mpiComm; // mpi parallel communicator
  const int mpiRank; // number of processors involved

  const int uRank;
  const int mRank;
  const int hRank;


  boost::shared_ptr<PostProcVolumeOnRefinedMesh> postProc;
  boost::shared_ptr<Monitor> monitorPtr;


  boost::shared_ptr<PreviousData> preData;

  boost::shared_ptr<MatrixDouble> dispGradPtr;
  boost::shared_ptr<MatrixDouble> dispValuePtr;

  boost::shared_ptr<MatrixDouble> fluxValuePtr;
  boost::shared_ptr<VectorDouble> massValuePtr;
  boost::shared_ptr<VectorDouble> massDotPtr;
  boost::shared_ptr<VectorDouble> fluxDivPtr;

};

// FTensor::Index<'i', 3> i;
// FTensor::Index<'j', 3> j;
// FTensor::Index<'k', 3> k;

struct StressAssistedDiffusivityFunction{
FTensor::Tensor2<double, 3, 3> operator()(FTensor::Tensor2_symmetric<double,3> t_stress, 
                                          const double                         c0,
                                          const double                         c1,
                                          const double                         c2) const {

  // MatrixDouble kron; kron.resize(3, 3, false); kron.clear(); 
  // kron(0, 0) = 1.0; kron(1, 1) = 1.0; kron(2, 2) = 1.0;

  // auto t_kron = getFTensor2FromMat<3, 3>(kron);
  FTensor::Tensor2<double, 3, 3> t_kron(1.0, 0.0, 0.0,
                                        0.0, 1.0, 0.0,
                                        0.0, 0.0, 1.0);

  FTensor::Tensor2<double, 3, 3> t_result;

  t_result(i, j) = c0 * t_kron(i, j) + c1 * t_stress(i, j) + c2 * t_stress(i, k) * t_stress(k, j);

  return t_result;
  }
};

struct DisplacementAssistedReactionFunction{
double operator() (FTensor::Tensor2<double *, 3, 3> t_dispGrad, const double d3){
  double divDisp = d3 * (t_dispGrad(0, 0) + t_dispGrad(1, 1) + t_dispGrad(2, 2));
  return divDisp;
}
};

struct Rhs_m {
  double operator()(const double m) const {
    return  m * (1.0 - m);
  }
};

struct ConcentrationAssistedForceFunction{
  FTensor::Tensor2<double, 3, 3> operator() (const double m, const double d) const {
    double cop = d * m;
    FTensor::Tensor2<double, 3, 3> force(cop, 0.0, 0.0,
                                         0.0, cop, 0.0,
                                         0.0, 0.0, cop);
    
    return  force;
  }
};

struct DiffConcentrationAssistedForceFunction {
  FTensor::Tensor2<double, 3, 3> operator()(const double m, const double d) const {
    FTensor::Tensor2<double, 3, 3> force(d, 0.0, 0.0,
                                         0.0, d, 0.0,
                                         0.0, 0.0, d);
    return  force;
  }
};

struct BodySourceFunction {
  FTensor::Tensor1<double, 3> operator() (const double x, 
                                          const double y, 
                                          const double z, 
                                          const double t) const {
    return FTensor::Tensor1<double, 3>(0., 0., 0.);
  }
};
struct BoundaryEssentialFunction {
  FTensor::Tensor1<double, 3> operator() (const double x, 
                                          const double y, 
                                          const double z, 
                                          const double t) const {
    double theta = .5 * M_PI;
    FTensor::Tensor1<double, 3> rotate(0.0, 0.0, 0.0);

    rotate(0) = 0.0;//0.5 * t; // t;
    rotate(1) = 0.1 * t;//(y * cos(t * theta) - z * sin(t * theta)) - y;
    rotate(2) = -0.1 * t;//(y * sin(t * theta) + z * cos(t * theta)) - z;

    return rotate;
  }
};

struct BoundaryForceFunction {
  FTensor::Tensor1<double, 3> operator()(const double x, 
                                         const double y,
                                         const double z, 
                                         const double t) const {
    double theta = .5 * M_PI;
    FTensor::Tensor1<double, 3> rotate(0.0, 0.0, 0.0);

    rotate(0) = 0.0;//-0.1 * t;   // t;
    rotate(1) = 0.0;//10. * (y * cos(t * theta) - z * sin(t * theta));
    rotate(2) = 1.0 * t; //10. * (y * sin(t * theta) + z * cos(t * theta));

    return rotate;
  }
};

CoupledProblem::CoupledProblem(moab::Core &mb_instance, MoFEM::Core &core)
: moab(mb_instance)
, mField(core)
, mpiComm(mField.get_comm())
, mpiRank(mField.get_comm_rank()) 
, uRank(3)
, mRank(1)
, hRank(1)
{
  volPipelineLHS = boost::shared_ptr<VolEle>(new VolEle(mField));
  facePipelineLHS = boost::shared_ptr<FaceEle>(new FaceEle(mField));

  volPipelineNStiffRHS = boost::shared_ptr<VolEle>(new VolEle(mField));
  
  volPipelineStiffRHS = boost::shared_ptr<VolEle>(new VolEle(mField));
  facePipelineStiffRHS = boost::shared_ptr<FaceEle>(new FaceEle(mField));

  postProc = boost::shared_ptr<PostProcVolumeOnRefinedMesh>(
      new PostProcVolumeOnRefinedMesh(mField));

  preData = boost::shared_ptr<PreviousData>(new PreviousData());

  dispGradPtr = boost::shared_ptr<MatrixDouble>(preData, &preData->dispGrad);
  dispValuePtr = boost::shared_ptr<MatrixDouble>(preData, &preData->dispValue);

  massValuePtr = boost::shared_ptr<VectorDouble>(preData, &preData->massValue);
  massDotPtr = boost::shared_ptr<VectorDouble>(preData, &preData->massDot);
  fluxValuePtr = boost::shared_ptr<MatrixDouble>(preData, &preData->fluxValue);
  fluxDivPtr = boost::shared_ptr<VectorDouble>(preData, &preData->fluxDiv);

  prob = 0;
  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-problem", &prob, PETSC_NULL);
}

MoFEMErrorCode CoupledProblem::readMesh() {
  MoFEMFunctionBegin;
  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  CHKERR simpleInterface->loadFile();
  MoFEMFunctionReturn(0);
}


MoFEMErrorCode CoupledProblem::setupSystem() {
  MoFEMFunctionBegin;
  if (prob == 0 || prob == 2) {
    CHKERR simpleInterface->addDomainField("U", H1, AINSWORTH_LEGENDRE_BASE,
                                           uRank);
    CHKERR simpleInterface->addBoundaryField("U", H1, AINSWORTH_LEGENDRE_BASE,
                                             uRank);
    int uOrder = 1;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-u_order", &uOrder, PETSC_NULL);
    CHKERR simpleInterface->setFieldOrder("U", uOrder);
  }

  if (prob == 1 || prob == 2) {
    CHKERR simpleInterface->addDomainField("FLUX", HDIV, DEMKOWICZ_JACOBI_BASE,
                                           hRank);
    CHKERR simpleInterface->addDomainField("MASS", L2, AINSWORTH_LEGENDRE_BASE,
                                           mRank);

    CHKERR simpleInterface->addBoundaryField("FLUX", HDIV,
                                             DEMKOWICZ_JACOBI_BASE, hRank);
    // CHKERR simpleInterface->addBoundaryField("MASS", L2,
    // DEMKOWICZ_JACOBI_BASE, mRank);

    int fOrder = 2;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-flux_order", &fOrder,
                              PETSC_NULL);

    CHKERR simpleInterface->setFieldOrder("FLUX", fOrder);
    CHKERR simpleInterface->setFieldOrder("MASS", fOrder - 1);
  }

    CHKERR simpleInterface->setUp();

    MoFEMFunctionReturn(0);
}

MoFEMErrorCode CoupledProblem::setIntegrationRule(){
  MoFEMFunctionBegin;
  auto vol_rule = [](int, int, int p) -> int { return 2 * p; };
  auto face_rule = [](int, int, int p) -> int { return 2 * p;};
  volPipelineLHS->getRuleHook = vol_rule;
  facePipelineLHS->getRuleHook = face_rule;
  volPipelineNStiffRHS->getRuleHook = vol_rule;
  volPipelineStiffRHS->getRuleHook = vol_rule;
  facePipelineStiffRHS->getRuleHook = face_rule;
 
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode CoupledProblem::setupUpdateCommonData() {
  MoFEMFunctionBegin;
  if (prob == 0 || prob == 2) {
    volPipelineStiffRHS->getOpPtrVector().push_back(
        new OpCalculateVectorFieldGradient<3, 3>("U", dispGradPtr));

    facePipelineStiffRHS->getOpPtrVector().push_back(
        new OpCalculateVectorFieldValues<3>("U", dispValuePtr));
  }
  if (prob == 2) {
    volPipelineNStiffRHS->getOpPtrVector().push_back(
        new OpCalculateVectorFieldGradient<3, 3>("U", dispGradPtr));

    // volPipelineStiffRHS->getOpPtrVector().push_back(
    //     new OpCalculateScalarFieldValues("MASS", massValuePtr));

    // volPipelineLHS->getOpPtrVector().push_back(
    //     new OpCalculateScalarFieldValues("MASS", massValuePtr));
  }

  if (prob == 1 || prob == 2) {
    volPipelineNStiffRHS->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues("MASS", massValuePtr));

    volPipelineStiffRHS->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues("MASS", massValuePtr));

    volPipelineStiffRHS->getOpPtrVector().push_back(
        new OpCalculateHdivVectorField<3>("FLUX", fluxValuePtr));

    volPipelineStiffRHS->getOpPtrVector().push_back(
        new OpCalculateScalarValuesDot("MASS", massDotPtr));

    volPipelineStiffRHS->getOpPtrVector().push_back(
        new OpCalculateHdivVectorDivergence<3, 3>("FLUX", fluxDivPtr));

    volPipelineLHS->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues("MASS", massValuePtr));

  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode CoupledProblem::setupPipeline() {
  MoFEMFunctionBegin;
  std::map<int, BlockData> block_map;

  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
    string name = it->getName();
    const int id = it->getMeshsetId();
    if (name.compare(0, 11, "MAT_ELASTIC") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 3, block_map[id].block_ents, true);
      std::vector<double> mat_properties;
      it->getAttributes(mat_properties);
      double B = mat_properties[0];
      double E = mat_properties[1];
      double nu = mat_properties[2];
      CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-conduct", &B, PETSC_NULL);
      CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-young", &E, PETSC_NULL);
      CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-poisson", &nu, PETSC_NULL);
      block_map[id].yOung = E;
      block_map[id].pOisson = nu;
      block_map[id].block_id = id;
    }
  }

  if (prob == 2) {

    volPipelineLHS->getOpPtrVector().push_back(
        new OpLHS_UV("U", "MASS", preData, block_map,
                     DiffConcentrationAssistedForceFunction()));
  }

  if (prob == 0 || prob == 2) {
    volPipelineStiffRHS->getOpPtrVector().push_back(new OpRHS_U(
        "U", preData, block_map, ConcentrationAssistedForceFunction()));

    // // 2. LHS
    volPipelineLHS->getOpPtrVector().push_back(
        new OpLHS_UU("U", preData, block_map));
  }


    //===========PUSH SLOW OPERATORS
    // SLOW RHS
    if (prob == 1 || prob == 2){
    volPipelineNStiffRHS->getOpPtrVector().push_back(
      new OpSlowRhsV("MASS", preData, Rhs_m(), DisplacementAssistedReactionFunction()));

    volPipelineStiffRHS->getOpPtrVector().push_back(
      new OpStiffRhsTau("FLUX", preData, block_map, StressAssistedDiffusivityFunction()));

    volPipelineStiffRHS->getOpPtrVector().push_back(
      new OpStiffRhsV("MASS", preData, block_map));

    volPipelineLHS->getOpPtrVector().push_back(
      new OpLhsTauTau("FLUX", preData, block_map, StressAssistedDiffusivityFunction()));

    volPipelineLHS->getOpPtrVector().push_back(
      new OpLhsTauV("FLUX", "MASS", block_map));

    volPipelineLHS->getOpPtrVector().push_back(
      new OpLhsVTau("MASS", "FLUX"));

    volPipelineLHS->getOpPtrVector().push_back(
      new OpLhsVV("MASS"));
    }

    MoFEMFunctionReturn(0);
  }

MoFEMErrorCode CoupledProblem::setupIC(){
  MoFEMFunctionBegin;
  if (prob == 1 || prob == 2) {
    auto volPiplineIC = boost::shared_ptr<VolEle>(new VolEle(mField));

    Range initialEnts;
    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
      string name = it->getName();
      const int id = it->getMeshsetId();
      if (name.compare(0, 7, "INITIAL") == 0) {
        CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
            id, BLOCKSET, 3, initialEnts, true);
        std::vector<double> initVal;
        it->getAttributes(initVal);
        volPiplineIC->getOpPtrVector().push_back(
            new OpInitialMass("MASS", initialEnts, initVal[0]));
      }
    }
    CHKERR DMoFEMLoopFiniteElements(dM, simpleInterface->getDomainFEName(),
                                    volPiplineIC);
  }
    MoFEMFunctionReturn(0);
  }

MoFEMErrorCode CoupledProblem::setupBC() {
  MoFEMFunctionBegin;
  auto get_boundary_ents = [&](std::string boundary_name){
  Range boundary_entities;
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
    std::string entity_name = it->getName();
    int name_size = boundary_name.size();
    if (entity_name.compare(0, name_size, boundary_name) == 0) {
      CHKERR it->getMeshsetIdEntitiesByDimension(mField.get_moab(), 2,
                                                  boundary_entities, true);
    }
  }
  return boundary_entities;

};

auto merge_adjacencies = [&](Range &manifod_ents) {
  Range adj_vertices;
  CHKERR moab.get_adjacencies(manifod_ents, 0, false, adj_vertices,
                              moab::Interface::UNION);
  Range adj_edges;
  CHKERR moab.get_adjacencies(manifod_ents, 1, false, adj_edges,
                              moab::Interface::UNION);

  manifod_ents.merge(adj_vertices);
  manifod_ents.merge(adj_edges);
};

auto get_boundary_markers = [&](Range &bound_ents) {
  auto problem_manager = mField.getInterface<ProblemsManager>();
  auto marker_ptr = boost::make_shared<std::vector<unsigned int>>();
  problem_manager->markDofs(simpleInterface->getProblemName(), ROW,
                            bound_ents, *marker_ptr);
  return marker_ptr;
};




if (prob == 0 || prob == 2){
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
    Range current_ents;
    std::string entity_name = it->getName();
    if (entity_name.compare(0, 5, "FIX_U") == 0) {
      CHKERR it->getMeshsetIdEntitiesByDimension(mField.get_moab(), 2,
                                                  current_ents, true);
      std::vector<double> bcVals;
      it->getAttributes(bcVals);

      for(int cc = 0; cc < 3; ++cc){
        Range boundary_ents;
        if(bcVals[2 * cc + 0] < 0)
          continue;
        int comp = (int)bcVals[2 * cc + 0];
        double val = bcVals[2 * cc + 1];
        
        boundary_ents.merge(current_ents);

        facePipelineStiffRHS->getOpPtrVector().push_back(new OpEssentialRHS_U(
            "U", preData, uRank, comp, val, boundary_ents));

        facePipelineLHS->getOpPtrVector().push_back(new OpBoundaryMassMatrix(
            "U", uRank, comp, boundary_ents));

        Range adj_vertices;
        CHKERR moab.get_adjacencies(boundary_ents, 0, false, adj_vertices,
                                    moab::Interface::UNION);
        Range adj_edges;
        CHKERR moab.get_adjacencies(boundary_ents, 1, false, adj_edges,
                                    moab::Interface::UNION);
        boundary_ents.merge(adj_vertices);
        boundary_ents.merge(adj_edges);

        auto marker_current_ents = get_boundary_markers(boundary_ents);

        volPipelineLHS->getOpPtrVector().push_back(
            new OpBCConstraint("U", comp, uRank, marker_current_ents));

        volPipelineStiffRHS->getOpPtrVector().push_back(new OpBCConstraint(
            "U", comp, uRank, marker_current_ents)); 
      }
    }
  }
    
}

  auto flux_boundary = get_boundary_ents("FIX_FLUX");
  if (prob == 1 || prob == 2) {
    double fluxBCValue = 0.0;
    facePipelineStiffRHS->getOpPtrVector().push_back(
        new OpFluxEssentialBC("FLUX", flux_boundary, fluxBCValue));

    merge_adjacencies(flux_boundary);

    // auto marker_flux_boundary = get_boundary_markers(flux_boundary);

    CHKERR mField.getInterface<ProblemsManager>()->removeDofsOnEntities(
        "SimpleProblem", "FLUX", flux_boundary);
  }
    MoFEMFunctionReturn(0);
  }

MoFEMErrorCode CoupledProblem::assembleSystem(){
  MoFEMFunctionBegin;
 
  CHKERR TSSetType(tS, TSARKIMEX);
  CHKERR TSARKIMEXSetType(tS, TSARKIMEXA2);


  boost::shared_ptr<ForcesAndSourcesCore> null;

  CHKERR DMMoFEMTSSetIJacobian(dM, simpleInterface->getBoundaryFEName(),
                               facePipelineLHS, null, null); 
  CHKERR DMMoFEMTSSetIJacobian(dM, simpleInterface->getDomainFEName(),
                               volPipelineLHS, null, null);

  CHKERR DMMoFEMTSSetIFunction(dM, simpleInterface->getBoundaryFEName(),
                               facePipelineStiffRHS, null, null);
  CHKERR DMMoFEMTSSetIFunction(dM, simpleInterface->getDomainFEName(),
                               volPipelineStiffRHS, null, null);
  CHKERR DMMoFEMTSSetRHSFunction(dM, simpleInterface->getDomainFEName(),
                                 volPipelineNStiffRHS, null, null);

  



  MoFEMFunctionReturn(0);
}

MoFEMErrorCode CoupledProblem::solveSystem(){
  MoFEMFunctionBegin;
  // Create solution vector
  SmartPetscObj<Vec> X;
  CHKERR DMCreateGlobalVector_MoFEM(dM, X);
  CHKERR DMoFEMMeshToLocalVector(dM, X, INSERT_VALUES, SCATTER_FORWARD);
  // Solve problem
  double ftime = 1;
  CHKERR TSSetDM(tS, dM);
  CHKERR TSSetMaxTime(tS, ftime);
  CHKERR TSSetSolution(tS, X);
  CHKERR TSSetFromOptions(tS);

  CHKERR TSSolve(tS, X);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode CoupledProblem::setOutput(){
  MoFEMFunctionBegin;
  postProc->generateReferenceElementMesh();

  if (prob == 0 || prob == 2)
    postProc->addFieldValuesPostProc("U");

  if (prob == 1 || prob == 2) {
    postProc->addFieldValuesPostProc("MASS");
    postProc->addFieldValuesPostProc("FLUX");
  }

  monitorPtr = boost::shared_ptr<Monitor>(
    new Monitor(mpiComm, mpiRank, dM, mField, postProc));
  boost::shared_ptr<ForcesAndSourcesCore> null;
  CHKERR DMMoFEMTSSetMonitor(dM, tS, simpleInterface->getDomainFEName(),
                             monitorPtr, null, null);

  MoFEMFunctionReturn(0);
  }

MoFEMErrorCode CoupledProblem::runAnalysis(){
  MoFEMFunctionBegin;
  CHKERR readMesh();
  CHKERR setupSystem();
  dM = simpleInterface->getDM();
  tS = createTS(mField.get_comm());
  CHKERR setIntegrationRule();
  CHKERR setupIC();
  CHKERR setupUpdateCommonData();
  CHKERR setupBC();
  CHKERR setupPipeline(); 
  CHKERR assembleSystem();

  CHKERR setOutput();
  CHKERR solveSystem();
  
  MoFEMFunctionReturn(0);
}

int main(int argc, char *argv[]) {
  const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, param_file, help);
  try {
    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    CoupledProblem coupled_problem(mb_instance, core);
    CHKERR coupled_problem.runAnalysis();
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}

#ifndef __BOUNDARYCONDITIONS_HPP__
#define __BOUNDARYCONDITIONS_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>

namespace BoundaryConditions {

using VolEle = MoFEM::VolumeElementForcesAndSourcesCore;
using OpVolEle = VolEle::UserDataOperator;

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using OpFaceEle = FaceEle::UserDataOperator;

using EdgeEle = MoFEM::FaceElementForcesAndSourcesCore;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

struct EssentialBC {
  EssentialBC(std::string         boundary_filed_name)
  {

  }

  private : 
    std::vector<std::string> boundary_field_names; 
};


}; // namespace ChemoMechOps

#endif //__BOUNDARYCONDITIONS_HPP__

auto pre_proc = [&]() -> MoFEMErrorCode {
  MoFEMFunctionBegin;
  struct SetCoords : EntityMethod {
    using EntityMethod::EntityMethod;

    MoFEMErrorCode preProcess() { return 0; }

    MoFEMErrorCode operator()() {
      MoFEMFunctionBegin;
      auto ent = entPtr->getEnt();
      VectorDouble3 coords(3);

      CHKERR mField.get_moab().get_coords(&ent, 1, &*coords.begin());

      coords += entPtr->getFieldData();
      CHKERR mField.get_moab().get_coords(&ent, 1, &*coords.begin());
      MoFEMFunctionReturn(0);
    }
    MoFEMErrorCode postProcess() { return 0; }
  };
  Range verts;
  SetCoords set_coords;
  CHKERR m_field.get_moab().get_entities_by_type(0, MBVERTEX, verts);
  CHKERR m_field.loop_entities("FIELD1", set_coords, &verts);
};
auto operator= [&]() -> MoFEMErrorCode { return 0; };
auto post_proc = [&]() -> MoFEMErrorCode { return 0; };


auto fe_set_coords = boost::make_shared<FEMethod>();
fe_set_coords->preProcessHook = pre_proc;
fe_set_coords->operatorHook = eval_error_op;
fe_set_coords->postProcessHook = post_proc;
CHKERR DMMoFEMTSSetIFunction(dm, simple_interface->getDomainFEName(), null,
                             fe_set_coords, null);

MatZeroRowsIS(B, vel, 0.0, NULL, NULL);


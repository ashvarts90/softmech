struct OpLHS_PU : OpVolEle {
  OpLHS_PU(std::string pres_name, std::string disp_name,
           boost::shared_ptr<PreviousData> &commonData,
           std::map<int, BlockData> &block_map)
      : OpVolEle(pres_name, disp_name, OpVolEle::OPROWCOL),
        setOfBlock(block_map), commonData(commonData) {
    sYmm = false;
  }

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {

      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      block_data.getMatParam();

      auto extract_col = [](MatrixDouble &m, const int r, const int c) {
        return FTensor::Tensor1<double *, 3>(&m(r, c + 0), &m(r, c + 1),
                                             &m(r, c + 2));
      };

      locMat.resize(nb_row_dofs, nb_col_dofs, false);
      locMat.clear();

      const int nb_integration_pts = getGaussPts().size2();

      auto t_row_val = row_data.getFTensor0N();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        for (int rr = 0; rr != nb_row_dofs; ++rr) {

          
          auto t_col_diff = col_data.getFTensor1DiffN<3>(gg, 0);

          for (int cc = 0; cc != nb_col_dofs / 3; ++cc) {
            auto t_subLocMat = extract_col(locMat, rr, 3 * cc);
            t_subLocMat(i) += -a * t_row_val * t_col_diff(i);
            ++t_col_diff;
          }
          ++t_row_val;
        }
        ++t_w;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data,
                          &locMat(0, 0), ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  MatrixDouble locMat;
  std::map<int, BlockData> setOfBlock;
};

  struct OpLHS_PP : OpVolEle {
    OpLHS_PP(std::string pres_name, boost::shared_ptr<PreviousData> &commonData,
             std::map<int, BlockData> &block_map)
        : OpVolEle(pres_name, pres_name, OpVolEle::OPROWCOL),
          setOfBlock(block_map), commonData(commonData) {
      sYmm = true;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data) {
      MoFEMFunctionBegin;
      const int nb_row_dofs = row_data.getIndices().size();
      const int nb_col_dofs = col_data.getIndices().size();

      if (nb_row_dofs && nb_col_dofs) {

        auto find_block_data = [&]() {
          EntityHandle fe_ent = getFEEntityHandle();
          BlockData *block_raw_ptr = nullptr;
          for (auto &m : setOfBlock) {
            if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
              block_raw_ptr = &m.second;
              break;
            }
          }
          return block_raw_ptr;
        };

        auto block_data_ptr = find_block_data();
        if (!block_data_ptr)
          SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
        auto &block_data = *block_data_ptr;

        block_data.getMatParam();

        bool is_incompressible = (std::abs(block_data.pOisson - 0.5) < 1e-6);

        double lambda_inv = is_incompressible ? 0. : 1.0 / block_data.lAmbda;

        locMat.resize(nb_row_dofs, nb_col_dofs, false);
        locMat.clear();

        const int nb_integration_pts = getGaussPts().size2();

        auto t_row_val = row_data.getFTensor0N();

        auto t_w = getFTensor0IntegrationWeight();
        const double vol = getMeasure();

        for (int gg = 0; gg != nb_integration_pts; ++gg) {
          const double a = vol * t_w;

          for (int rr = 0; rr != nb_row_dofs; ++rr) {

            auto t_col_val = col_data.getFTensor0N(gg, 0);

            for (int cc = 0; cc != nb_col_dofs; ++cc) {
              locMat(rr, cc) += -a * t_row_val * t_col_val * lambda_inv;
              ++t_col_val;
            }
            ++t_row_val;
          }
          ++t_w;
        }
        CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data,
                            &locMat(0, 0), ADD_VALUES);

        if (row_side != col_side || row_type != col_type) {
          locTransMat.resize(nb_col_dofs, nb_row_dofs, false);
          noalias(locTransMat) = trans(locMat);
          CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
                              &locTransMat(0, 0), ADD_VALUES);
        }
      }
      MoFEMFunctionReturn(0);
    }

  private:
    boost::shared_ptr<PreviousData> commonData;
    MatrixDouble locMat, locTransMat;
    Range essentialBdryEnts;
    std::map<int, BlockData> setOfBlock;
  };


struct OpLHS_UP : OpVolEle {
  OpLHS_UP(std::string                          disp_name, 
           std::string                          pres_name,
           boost::shared_ptr<PreviousData>      &commonData,
           std::map<int, BlockData>             &block_map,
           Range                                &essential_ents,
           boost::shared_ptr<std::vector<bool>> boundary_markerx = nullptr,
           boost::shared_ptr<std::vector<bool>> boundary_markery = nullptr,
           boost::shared_ptr<std::vector<bool>> boundary_markerz = nullptr)
      : OpVolEle(disp_name, pres_name, OpVolEle::OPROWCOL)
      , setOfBlock(block_map)
      , commonData(commonData)
      , boundaryMarkerX(boundary_markerx)
      , boundaryMarkerY(boundary_markery)
      , boundaryMarkerZ(boundary_markerz) 
      , essentialEnts(essential_ents)
      {
        sYmm = false;
      }

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {
      //  EntityHandle ent = getNumeredEntFiniteElementPtr()->getEnt();
      // // getFEEntityHandle();

      // if (essentialEnts.find(ent) == essentialEnts.end()) {
      //    MoFEMFunctionReturnHot(0);
      // }

      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      block_data.getMatParam();

      auto extract_col = [](MatrixDouble &m, const int r, const int c) {
        return FTensor::Tensor1<double *, 3>(
            &m(r + 0, c), &m(r + 1, c), &m(r + 2, c));
      };

      locMat.resize(nb_row_dofs, nb_col_dofs, false);
      locMat.clear();

      const int nb_integration_pts = getGaussPts().size2();

      auto t_row_diff = row_data.getFTensor1DiffN<3>();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        for (int rr = 0; rr != nb_row_dofs / 3; ++rr) {

          auto t_col_val = col_data.getFTensor0N(gg, 0);

          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            auto t_subLocMat = extract_col(locMat, 3 * rr, cc);
            t_subLocMat(i) += -a * t_col_val * t_row_diff(i);
            ++t_col_val;
          }
          ++t_row_diff;
        }
        ++t_w;
      }
      // auto row_indices = row_data.getIndices();

      // CHKERR flg_indicies(row_data, boundaryMarkerX, nb_row_dofs / 3, 0);
      // CHKERR flg_indicies(row_data, boundaryMarkerY, nb_row_dofs / 3, 1);
      // CHKERR flg_indicies(row_data, boundaryMarkerZ, nb_row_dofs / 3, 2);

      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data,
                          &locMat(0, 0), ADD_VALUES);

      // row_data.getIndices().data().swap(row_indices.data());

      // locTransMat.resize(nb_col_dofs, nb_row_dofs, false);
      locTransMat = trans(locMat);
      CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
                          &locTransMat(0, 0), ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData>      commonData;
  MatrixDouble                         locMat, locTransMat;
  std::map<int, BlockData>             setOfBlock;
  Range                                essentialEnts;
  boost::shared_ptr<std::vector<bool>> boundaryMarkerX;
  boost::shared_ptr<std::vector<bool>> boundaryMarkerY;
  boost::shared_ptr<std::vector<bool>> boundaryMarkerZ;
};

struct OpRHS_P : OpVolEle {
  OpRHS_P(std::string                          pres_name, 
          boost::shared_ptr<PreviousData>      &commonData,
          std::map<int, BlockData>             &block_map)
      : OpVolEle(pres_name, OpVolEle::OPROW)
      , setOfBlock(block_map)
      , commonData(commonData){}

  MoFEMErrorCode doWork(int        row_side, 
                        EntityType row_type, 
                        EntData    &row_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();

    if (nb_row_dofs) {

      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      auto t_dispGrad = getFTensor2FromMat<3, 3>(commonData->dispGrad);
      auto t_presValue = getFTensor0FromVec(commonData->pressureValue);

      block_data.getMatParam();


      locRes.resize(nb_row_dofs, false);
      locRes.clear();

      const int nb_integration_pts = getGaussPts().size2();

      auto t_row_val = row_data.getFTensor0N();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();


      bool is_incompressible = (std::abs(block_data.pOisson - 0.5) < 1e-6);

      double lambda_inv = is_incompressible ? 0. : 1.0 / block_data.lAmbda;

   

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        double div_u = t_dispGrad(i, i);
        for (int rr = 0; rr != nb_row_dofs; ++rr) {


          locRes(rr) += - a * t_row_val * (lambda_inv * t_presValue + div_u);

          ++t_row_val;
        }
        ++t_dispGrad;
        ++t_presValue;
        ++t_w;
      }

      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*locRes.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  VectorDouble locRes;
  VectorFunc    dispFunction;
  std::map<int, BlockData> setOfBlock;
};

CHKERR DMoFEMGetInterfacePtr(dm, &m_field);
for (_IT_GET_ENT_FIELD_BY_NAME_FOR_LOOP_(m_field, "DISPLACEMENTS", it)) {
  if (it->get()->getEntType() == MBVERTEX) {

    EntityHandle ent = it->get()->getEnt();
    VectorDouble3 coords(3);

    CHKERR mField.get_moab().get_coords(&ent, 1, &*coords.begin());
    for (int dd = 0; dd != 3; ++dd)
      coords[dd] += it->get()->getEntFieldData()[dd];
    CHKERR mField.get_moab().set_coords(&ent, 1, &*coords.begin());
  }
}
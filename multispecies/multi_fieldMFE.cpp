#include <stdlib.h>
#include <BasicFiniteElements.hpp>

int g_max_order = 1;
double global_error0;
double global_error1;
double post_error;
double maxError;

#include <RDOperators.hpp>
// #include <ErrorEstimateDrafts.hpp>

using namespace MoFEM;
using namespace ReactionDiffusion;
using namespace ErrorEstimates;

static char help[] = "...\n\n";

// #define M_PI 3.14159265358979323846 /* pi */
template<int nbSpecies>
struct RDProblem {
public:
  RDProblem(MoFEM::Core &core);

  // RDProblem(const int order) : order(order){}
  MoFEMErrorCode runAnalysis(int nb_sp);

private:
  MoFEMErrorCode     readMesh();
  MoFEMErrorCode     setupSystem();
  MoFEMErrorCode     setIntegrationRule();

  MoFEMErrorCode     setupIC();
  MoFEMErrorCode     setupBC();
  MoFEMErrorCode     setupPipeline();
  
  MoFEMErrorCode     setupUpdateCommonData();
  
  MoFEMErrorCode     assembleSystem();
  MoFEMErrorCode     solveSystem();
  MoFEMErrorCode     setOutput();

  boost::shared_ptr<FaceEle>     facePipelineNStiffRhs;
  boost::shared_ptr<FaceEle>     facePipelineStiffRhs;
  boost::shared_ptr<FaceEle>     facePipelineLhs;

  boost::shared_ptr<EdgeEle>     skeletonPipeline;
  boost::shared_ptr<FaceEle>     facePipelineMaxPostError;

  MoFEM::Interface     &mField;
  Simple               *simpleInterface;
  SmartPetscObj<DM>    dM;
  SmartPetscObj<TS>    tS;
  MPI_Comm             mpiCmm;
  const int            mpiRank;


  std::map<int, BlockData> materialBlocks;
  std::map<int, Range>     innerSurfaces;
  Range                    essentialBdryEnts;



  boost::shared_ptr<PostProcFaceOnRefinedMeshFor2D> postProcFE;
  boost::shared_ptr<Monitor>                        monitorPtr;


  
  std::vector<boost::shared_ptr<PreviousData>>      dataAtGP;
  std::vector<boost::shared_ptr<MatrixDouble>>      fluxValuePtrs;
  std::vector<boost::shared_ptr<VectorDouble>>      fluxDivesPtrs;
  std::vector<boost::shared_ptr<VectorDouble>>      massValuesPtrs;
  std::vector<boost::shared_ptr<VectorDouble>>      massDotsPtrs;
};

const double ramp_t = 1.0;
const double sml = 0.0;
const double T = M_PI / 2.0;
const double R = 0.75;

struct KinkFunction {
  double operator() (const double x) const {
    return 1 - abs(x);
  }
};

struct DerKinkFunction{
  double operator()(const double x) const {
    if(x > 0)
      {return -1;}
    else 
      {return 1;}
}
};

struct ExactFunction {
  double operator()(const double x, const double y, const double t) const {
    // double g = cos(T * x) * cos(T * y);

    // //##################################################
    double g = 0;
    double r = x * x + y * y;
    double d = R * R - r;

    if(d > 0.0){
      g = exp(-r / d);
    }
    // //###################################################
    if (t <= ramp_t) {
      return g * t;
    } else {
      return g * ramp_t;
    }
  }
};

struct ExactFunctionGrad {
  FTensor::Tensor1<double, 3> operator()(const double x, const double y,
                                         const double t) const {
    FTensor::Tensor1<double, 3> grad;
    // double mx = - T * sin(T * x) * cos(T * y);
    // double my = - T * cos(T * x) * sin(T * y);

    //##################################################
    double mx = 0.0;
    double my = 0.0;
    double r = x * x + y * y;
    double d = R * R - r;

    if (d > 0.0) {
      double rx = 2.0 * x;
      double ry = 2.0 * y;

      double g = exp(-r / d);

      mx = g * (- rx * R * R / (d * d));
      my = g * (- ry * R * R / (d * d));
    }
    //##################################################

    if (t <= ramp_t) {
      grad(0) = mx * t;
      grad(1) = my * t;
    } else {
      grad(0) = mx * ramp_t;
      grad(1) = my * ramp_t;
    }
    grad(2) = 0.0;
    return grad;
  }
};

struct ExactFunctionLap {
  double operator()(const double x, const double y, const double t) const {
    // double glap = -2.0 * pow(T, 2) * cos(T * x) * cos(T * y);

    // //##################################################
    double glap = 0.0;
    double r = x * x + y * y;
    double d = R * R - r;
    if (d > 0.0) {
      double rx = 2.0 * x;
      double ry = 2.0 * y;

      double rxx = 2.0;
      double ryy = 2.0;
      double g = exp(-r / d);

      double cof = R * R / (d * d);
      glap = g * ((-rx * cof) * (-rx * cof) + (-ry * cof) * (-ry * cof) 
                  - R * R * (rxx * d + 2.0 * rx * rx) / (d * d * d) 
                  - R * R * (ryy * d + 2.0 * ry * ry) / (d * d * d));
    }
    // //##################################################
    if (t <= ramp_t) {
      return glap * t;
    } else {
      return glap * ramp_t;
    }
  }
};

struct ExactFunctionDot {
  double operator()(const double x, const double y, const double t) const {
    // double gdot = cos(T * x) * cos(T * y);
    // //##################################################
    double gdot = 0;
    double r = x * x + y * y;
    double d = R * R - r;

    if (d > 0.0) {
      gdot = exp(-r / d);
    }
    // //##################################################
    if (t <= ramp_t) {
      return gdot;
    } else {
      return 0;
    }
  }
};

// member function implementations

template<int nbSpecies> 
RDProblem<nbSpecies>::RDProblem(MoFEM::Core &core)
: mField(core)
, mpiComm(mField.get_comm())
, mpiRank(mField.get_comm_rank())
{
  facePipelineNStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(mField));
  facePipelineStiffRhs  = boost::shared_ptr<FaceEle>(new FaceEle(mField));
  facePipelineLhs       = boost::shared_ptr<FaceEle>(new FaceEle(mField));

  skeletonPipeline         = boost::shared_ptr<EdgeEle>(new EdgeEle(mField));
  facePipelineMaxPostError = boost::shared_ptr<FaceEle>(new FaceEle(mField));

  postProcFE = boost::shared_ptr<PostProcFaceOnRefinedMeshFor2D>(
                              new PostProcFaceOnRefinedMeshFor2D(mField));

  for(int ns = 0; ns < nbSpecies; ns++){
    dataAtGP.push_back(boost::shared_ptr<PreviousData>(new PreviousData()));

    fluxValuePtrs.push_back(boost::shared_ptr<MatrixDouble>(dataAtGP[ns], &dataAtGP[ns]->flux_values));
    fluxDivesPtrs.push_back(boost::shared_ptr<VectorDouble>(dataAtGP[ns], &dataAtGP[ns]->flux_dives));
    massValuesPtrs.push_back(boost::shared_ptr<VectorDouble>(dataAtGP[ns], &dataAtGP[ns]->mass_values));
    massDotsPtrs.push_back(boost::shared_ptr<VectorDouble>(dataAtGP[ns], &dataAtGP[ns]->mass_dots));
  } 

}

template<int nbSpecies> 
MoFEMErrorCode RDProblem<nbSpecies>::readMesh(){
  MoFEMFunctionBegin;
  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  CHKERR simpleInterface->loadFile();
  MoFEMFunctionReturn(0);
}

template<int nbSpecies>
MoFEMErrorCode RDProblem<nbSpecies>::setupSystem(){
  CHKERR simpleInterface->addDomainField("M", L2, AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addDomainField("F", HCURL, DEMKOWICZ_JACOBI_BASE, 1);

  CHKERR simpleInterface->addSkeletonField("F", HCURL, DEMKOWICZ_JACOBI_BASE, 1);

  CHKERR simpleInterface->addDataField("PRERROR", L2, AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addDataField("POERROR", L2, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->setFieldOrder("M", g_max_order - 1);
  CHKERR simpleInterface->setFieldOrder("F", g_max_order);

  CHKERR simpleInterface->setFieldOrder("PRERROR", 0); // approximation order for error
  CHKERR simpleInterface->setFieldOrder("POERROR", 0);

  std::vector<std::string> mass_names(nb_species);
  std::vector<std::string> flux_names(nb_species);

  for (int i = 0; i < nb_species; ++i) {
    mass_names[i] = "MASS" + boost::lexical_cast<std::string>(i + 1);
  }

  for(int ns = 0; ns < nbSpecies; ns++){
    mass_names[ns] = "M" + boost::lexical_cast<std::string>(ns + 1);
    flux_names[ns] = "F" + boost::lexical_cast<std::string>(ns + 1);

    CHKERR simpleInterface->addDataField(mass_names[ns], L2, AINSWORTH_LEGENDRE_BASE, 1);
    CHKERR simpleInterface->addDataField(flux_names[ns], HCURL, DEMKOWICZ_JACOBI_BASE, 1);
    CHKERR simpleInterface->setFieldOrder(mass_names[ns], g_max_order - 1);
    CHKERR simpleInterface->setFieldOrder(flux_names[ns], g_max_order);
  }
}







MoFEMErrorCode RDProblem::setup_system() {
  MoFEMFunctionBegin;
  CHKERR m_field.getInterface(simple_interface);
  CHKERR simple_interface->getOptions();
  CHKERR simple_interface->loadFile();
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::add_fe(std::string mass_field,
                                 std::string flux_field) {
  MoFEMFunctionBegin;
  
  
  CHKERR simple_interface->addDomainField(mass_field, L2,
                                          AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simple_interface->addDomainField(flux_field, HCURL,
                                            DEMKOWICZ_JACOBI_BASE, 1);

  // CHKERR simple_interface->addDataField("U", HCURL, DEMKOWICZ_JACOBI_BASE, 1);

  // CHKERR simple_interface->addBoundaryField(flux_field, HCURL,
  //                                           DEMKOWICZ_JACOBI_BASE, 1);
  CHKERR simple_interface->addSkeletonField(flux_field, HCURL,
                                            DEMKOWICZ_JACOBI_BASE, 1);

  CHKERR simple_interface->addDataField("ERROR", L2, AINSWORTH_LEGENDRE_BASE,
                                        1);
  CHKERR simple_interface->addDataField("ERROR2", L2, AINSWORTH_LEGENDRE_BASE,
                                        1);

  CHKERR simple_interface->setFieldOrder(mass_field, g_max_order - 1);
  CHKERR simple_interface->setFieldOrder(flux_field, g_max_order);
  // CHKERR simple_interface->setFieldOrder("U", order - 1);
  CHKERR simple_interface->setFieldOrder("ERROR", 0); // approximation order for error
  CHKERR simple_interface->setFieldOrder("ERROR2", 0);

  MoFEMFunctionReturn(0);
}
MoFEMErrorCode RDProblem::set_blockData(std::map<int, BlockData> &block_map) {
  MoFEMFunctionBegin;
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
    string name = it->getName();
    const int id = it->getMeshsetId();
    if (name.compare(0, 14, "REGION1") == 0) {
      CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, block_map[id].block_ents, true);
      block_map[id].B0 = 1e-3;
      block_map[id].block_id = id;
    } else if (name.compare(0, 14, "REGION2") == 0) {
      CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, block_map[id].block_ents, true);
      block_map[id].B0 = 1e0;
      block_map[id].block_id = id;
    } else if (name.compare(0, 14, "REGION3") == 0) {
      CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, block_map[id].block_ents, true);
      block_map[id].B0 = 1e-4;
      block_map[id].block_id = id;
    } else if (name.compare(0, 14, "REGION4") == 0) {
      CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, block_map[id].block_ents, true);
      block_map[id].B0 = 1e-4;
      block_map[id].block_id = id;
    } else if (name.compare(0, 14, "REGION5") == 0) {
      CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, block_map[id].block_ents, true);
      block_map[id].B0 = 1e-4;
      block_map[id].block_id = id;
    }
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::extract_bd_ents(std::string essential,
                                          std::string natural,
                                          std::string internal) {
  MoFEMFunctionBegin;
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
    string name = it->getName();
    if (name.compare(0, 14, natural) == 0) {

      CHKERR it->getMeshsetIdEntitiesByDimension(m_field.get_moab(), 1,
                                                 natural_bdry_ents, true);
    } else if (name.compare(0, 14, essential) == 0) {
      CHKERR it->getMeshsetIdEntitiesByDimension(m_field.get_moab(), 1,
                                                 essential_bdry_ents, true);
    } else if (name.compare(0, 14, internal) == 0) {
      CHKERR it->getMeshsetIdEntitiesByDimension(m_field.get_moab(), 1,
                                                 internal_edge_ents, true);
    }
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::extract_initial_ents(int block_id, Range &surface) {
  MoFEMFunctionBegin;
  if (m_field.getInterface<MeshsetsManager>()->checkMeshset(block_id,
                                                            BLOCKSET)) {
    CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
        block_id, BLOCKSET, 2, surface, true);
  }
  MoFEMFunctionReturn(0);
}
MoFEMErrorCode
RDProblem::update_slow_rhs(std::string mass_field,
                           boost::shared_ptr<VectorDouble> &mass_ptr) {
  MoFEMFunctionBegin;
  vol_ele_slow_rhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(mass_field, mass_ptr, MBTRI));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::push_slow_rhs(std::string mass_field,
                                        std::string flux_field,
                                        boost::shared_ptr<PreviousData> &data) {
  MoFEMFunctionBegin;

  vol_ele_slow_rhs->getOpPtrVector().push_back(
      new OpAssembleSlowRhsV(mass_field, data, 
                             ExactFunction(), 
                             ExactFunctionDot(), 
                             ExactFunctionLap(),
                             m_field));

  // natural_bdry_ele_slow_rhs->getOpPtrVector().push_back(
  //     new OpSkeletonSource(mass_field, 
  //                          KinkFunction(),
  //                          ExactFunction(), 
  //                          internal_edge_ents));

  // natural_bdry_ele_slow_rhs->getOpPtrVector().push_back(
  //     new OpAssembleNaturalBCRhsTau(flux_field, natural_bdry_ents));

  // natural_bdry_ele_slow_rhs->getOpPtrVector().push_back(
  //     new OpEssentialBC(flux_field, essential_bdry_ents));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::update_vol_fe(boost::shared_ptr<FaceEle> &vol_ele,
                                        boost::shared_ptr<PreviousData> &data) {
  MoFEMFunctionBegin;
  vol_ele->getOpPtrVector().push_back(new OpCalculateJacForFace(data->jac));
  vol_ele->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(data->inv_jac));
  vol_ele->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());

  vol_ele->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformFace(data->jac));

  vol_ele->getOpPtrVector().push_back(new OpSetInvJacHcurlFace(data->inv_jac));
  MoFEMFunctionReturn(0);
}
MoFEMErrorCode
RDProblem::update_stiff_rhs(std::string mass_field, std::string flux_field,
                            boost::shared_ptr<VectorDouble> &mass_ptr,
                            boost::shared_ptr<MatrixDouble> &flux_ptr,
                            boost::shared_ptr<VectorDouble> &mass_dot_ptr,
                            boost::shared_ptr<VectorDouble> &flux_div_ptr) {

  MoFEMFunctionBegin;

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(mass_field, mass_ptr, MBTRI));

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorField<3>(flux_field, flux_ptr));

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpCalculateScalarValuesDot(mass_field, mass_dot_ptr, MBTRI));

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorDivergence<3, 2>(flux_field, flux_div_ptr));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::push_stiff_rhs(std::string mass_field,
                                         std::string flux_field,
                                         boost::shared_ptr<PreviousData> &data,
                                         std::map<int, BlockData> &block_map) {
  MoFEMFunctionBegin;
  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpAssembleStiffRhsTau<3>(flux_field, data, block_map, m_field));

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpAssembleStiffRhsV<3>(mass_field, data, block_map, ExactFunction(),
                                 ExactFunctionDot(), ExactFunctionLap(), m_field));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
RDProblem::update_stiff_lhs(std::string mass_field, std::string flux_field,
                            boost::shared_ptr<VectorDouble> &mass_ptr,
                            boost::shared_ptr<MatrixDouble> &flux_ptr) {
  MoFEMFunctionBegin;
  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(mass_field, mass_ptr));

  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorField<3>(flux_field, flux_ptr));    
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::push_stiff_lhs(std::string mass_field,
                                         std::string flux_field,
                                         boost::shared_ptr<PreviousData> &data,
                                         std::map<int, BlockData> &block_map) {
  MoFEMFunctionBegin;
  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpAssembleLhsTauTau<3>(flux_field, data, block_map, m_field));

  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpAssembleLhsVV(mass_field, data->maxOrder, m_field));

  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpAssembleLhsTauV<3>(flux_field, mass_field, data, block_map, m_field));

  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpAssembleLhsVTau(mass_field, flux_field, data->maxOrder, m_field));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::set_integration_rule() {
  MoFEMFunctionBegin;
  auto vol_ele_slow_rhs_rule = [&](int, int, int) -> int {
    auto fe_ent = vol_ele_slow_rhs->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(data1->quadRule, &fe_ent, 1, &quad_rule);

    return 2 * (quad_rule + 0);
  };
  vol_ele_slow_rhs->getRuleHook = vol_ele_slow_rhs_rule;

  // ====================================================

  auto natural_bdry_ele_slow_rhs_rule = [&](int, int, int) -> int {
    auto fe_ent = natural_bdry_ele_slow_rhs->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(data1->quadRule, &fe_ent, 1,
                                           &quad_rule);

    return 2 * (quad_rule + 0);
  };
  natural_bdry_ele_slow_rhs->getRuleHook = natural_bdry_ele_slow_rhs_rule;

  // ====================================================

  auto vol_ele_stiff_rhs_rule = [&](int, int, int) -> int {
    auto fe_ent = vol_ele_stiff_rhs->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(data1->quadRule, &fe_ent, 1,
                                           &quad_rule);

    return 2 * (quad_rule + 0);
  };
  vol_ele_stiff_rhs->getRuleHook = vol_ele_stiff_rhs_rule;

  // ====================================================
  auto vol_ele_stiff_lhs_rule = [&](int, int, int) -> int {
    auto fe_ent = vol_ele_stiff_lhs->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(data1->quadRule, &fe_ent, 1,
                                           &quad_rule);

    return 2 * (quad_rule + 0);
  };
  vol_ele_stiff_lhs->getRuleHook = vol_ele_stiff_lhs_rule;

  // ====================================================

  auto skeleton_fe_rule = [&](int, int, int) -> int {
    auto fe_ent = skeleton_fe->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(data1->maxOrder, &fe_ent, 1,
                                           &quad_rule);

    return 2 * (quad_rule + 0);
  };
  skeleton_fe->getRuleHook = skeleton_fe_rule;

  // ====================================================
  // domain_error_fe->getRuleHook = vol_rule;
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::apply_IC(std::string mass_field, Range &surface,
                                   boost::shared_ptr<PreviousData> &data,
                                   boost::shared_ptr<FaceEle> &initial_ele) {
  MoFEMFunctionBegin;
  initial_ele->getOpPtrVector().push_back(
      new OpInitialMass(mass_field, data, surface, m_field));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::apply_BC(std::string flux_field) {
  MoFEMFunctionBegin;
  CHKERR m_field.getInterface<ProblemsManager>()->removeDofsOnEntities(
      simple_interface->getProblemName(), flux_field, essential_bdry_ents);

  MoFEMFunctionReturn(0);
}
MoFEMErrorCode RDProblem::loop_fe() {
  MoFEMFunctionBegin;



  CHKERR TSSetType(ts, TSARKIMEX);
  CHKERR TSARKIMEXSetType(ts, TSARKIMEXA2);

  CHKERR DMMoFEMTSSetIJacobian(dm, simple_interface->getDomainFEName(),
                               vol_ele_stiff_lhs, null, null);


  CHKERR DMMoFEMTSSetIFunction(dm, simple_interface->getDomainFEName(),
                               vol_ele_stiff_rhs, null, null);

  CHKERR DMMoFEMTSSetRHSFunction(dm, simple_interface->getDomainFEName(),
                                 vol_ele_slow_rhs, null, null);

  CHKERR DMMoFEMTSSetRHSFunction(dm, simple_interface->getSkeletonFEName(),
                                 skeleton_fe, null, null);


  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::post_proc_fields(std::string mass_field,
                                           std::string flux_field) {
  MoFEMFunctionBegin;
  post_proc->addFieldValuesPostProc(mass_field);
  post_proc->addFieldValuesPostProc(flux_field);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::output_result() {
  MoFEMFunctionBegin;
  CHKERR DMMoFEMTSSetMonitor(dm, ts, simple_interface->getDomainFEName(),
                             monitor_ptr, null, null);
  MoFEMFunctionReturn(0);
}
MoFEMErrorCode RDProblem::solve() {
  MoFEMFunctionBegin;
  // Create solution vector
  SmartPetscObj<Vec> X;
  CHKERR DMCreateGlobalVector_MoFEM(dm, X);
  CHKERR DMoFEMMeshToLocalVector(dm, X, INSERT_VALUES, SCATTER_FORWARD);
  // Solve problem
  double ftime = 1;
  CHKERR TSSetDM(ts, dm);
  CHKERR TSSetMaxTime(ts, ftime);
  CHKERR TSSetSolution(ts, X);
  CHKERR TSSetFromOptions(ts);

  if (1) {
    SNES snes;
    CHKERR TSGetSNES(ts, &snes);
    KSP ksp;
    CHKERR SNESGetKSP(snes, &ksp);
    PC pc;
    CHKERR KSPGetPC(ksp, &pc);
    PetscBool is_pcfs = PETSC_FALSE;
    PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);
    // Set up FIELDSPLIT
    // Only is user set -pc_type fieldsplit
    if (is_pcfs == PETSC_TRUE) {
      IS is_mass, is_flux;
      const MoFEM::Problem *problem_ptr;
      CHKERR DMMoFEMGetProblemPtr(dm, &problem_ptr);
      CHKERR
      m_field.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, "MASS1", 0, 1, &is_mass);
      CHKERR
      m_field.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, "FLUX1", 0, 1, &is_flux);
      // CHKERR ISView(is_flux, PETSC_VIEWER_STDOUT_SELF);
      // CHKERR ISView(is_mass, PETSC_VIEWER_STDOUT_SELF);
      
      CHKERR PCFieldSplitSetIS(pc, NULL, is_mass);
      CHKERR PCFieldSplitSetIS(pc, NULL, is_flux);

      CHKERR ISDestroy(&is_flux);
      CHKERR ISDestroy(&is_mass);
  }
}

    // auto field_split_fun = [](TS ts_ctx) -> PetscErrorCode {
    //   MoFEMFunctionBegin;

    //   DM dm;
    //   CHKERR TSGetDM(ts_ctx, &dm);

    //   MoFEM::Interface *m_field_ptr;

    //   CHKERR DMoFEMGetInterfacePtr(dm, &m_field_ptr);

    //   SNES snes;
    //   CHKERR TSGetSNES(ts_ctx, &snes);
    //   KSP ksp;
    //   CHKERR SNESGetKSP(snes, &ksp);
    //   PC pc;
    //   CHKERR KSPGetPC(ksp, &pc);
    //   PetscBool is_pcfs = PETSC_FALSE;
    //   PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);

    //   if(is_pcfs){

    //     IS is_lo;
    //     IS is_hi;
    //     const MoFEM::Problem *problem_ptr;
    //     CHKERR DMMoFEMGetProblemPtr(dm, &problem_ptr);
    //     auto dofs = problem_ptr->getNumeredRowDofs();
    //     auto nb_local_dofs = problem_ptr->getNbLocalDofsRow();
    //     Tag tag;
    //     CHKERR m_field_ptr->get_moab().tag_get_handle("_ORDER", tag);

    //     std::vector<int> vec_is_lo;
    //     std::vector<int> vec_is_hi;

    //     vec_is_lo.reserve(dofs->size());
    //     vec_is_hi.reserve(dofs->size());

    //     for (auto &dof : *(dofs)) {
    //       auto loc_idx = dof->getPetscLocalDofIdx();

    //       if (loc_idx < nb_local_dofs) {
    //         auto ent = dof->getEnt();
    //         int tag_order;
    //         CHKERR m_field_ptr->get_moab().tag_get_data(tag, &ent, 1,
    //         &tag_order); auto o = dof->getDofOrder();

    //         bool correct_order = false;

    //         if(dof->getSpace() == L2){

    //             correct_order = (o < tag_order);
    //         } else {

    //             correct_order = (o <= tag_order);
    //         }

    //         if (correct_order){
    //           vec_is_lo.push_back(dof->getPetscGlobalDofIdx());
    //         } else {
    //           vec_is_hi.push_back(dof->getPetscGlobalDofIdx());
    //         }

    //       }
    //     }
    //     std::sort(vec_is_lo.begin(), vec_is_lo.end());
    //     CHKERR ISCreateGeneral(m_field_ptr->get_comm(), vec_is_lo.size(),
    //                            &*vec_is_lo.begin(), PETSC_COPY_VALUES,
    //                            &is_lo);
    //     CHKERR ISCreateGeneral(m_field_ptr->get_comm(), vec_is_hi.size(),
    //                            &*vec_is_hi.begin(), PETSC_COPY_VALUES,
    //                            &is_hi);

    //     CHKERR PCFieldSplitSetIS(pc, NULL, is_lo);
    //     CHKERR PCFieldSplitSetIS(pc, NULL, is_hi);

    //     PetscInt nsplits;
    //     KSP *sub_ksp;
    //     PC pc_lo;
    //     PC pc_hi;

    //     CHKERR PCFieldSplitGetSubKSP(pc, &nsplits, &sub_ksp);

    //     CHKERR KSPGetPC(sub_ksp[0], &pc_lo);
    //     CHKERR KSPGetPC(sub_ksp[1], &pc_hi);

    //     CHKERR PCSetType(pc_lo, PCLU);
    //     CHKERR PCFactorSetMatSolverPackage(pc_lo, MATSOLVERMUMPS);

    //     CHKERR PCSetType(pc_hi, PCJACOBI);

    //     CHKERR PetscFree(sub_ksp);
    //     CHKERR ISDestroy(&is_hi);
    //     CHKERR ISDestroy(&is_lo);
    //   }

    //   MoFEMFunctionReturn(0);
    // };

    // auto reset_fun = [](TS ts) -> PetscErrorCode {
    //   MoFEMFunctionBegin;
    //   // CHKERR TSReset(ts);
    //   SNES snes;
    //   CHKERR TSGetSNES(ts, &snes);
    //   KSP ksp;

    //   CHKERR SNESGetKSP(snes, &ksp);
    //   // CHKERR KSPReset(ksp);
    //   // CHKERR KSPSetFromOptions(ksp);
    //   PC pc;
    //   CHKERR KSPGetPC(ksp, &pc);
    //   PetscBool is_pcfs = PETSC_FALSE;
    //   PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);
    //   if(is_pcfs){
    //      CHKERR PCReset(pc);
    //     //  CHKERR PCSetFromOptions(pc);
    //   }
    //   MoFEMFunctionReturn(0);
    // };

    // CHKERR TSSetPostEvaluate(ts, reset_fun);
    // CHKERR TSSetPreStep(ts, field_split_fun);

    // auto ij_jacobian = [](TS ts, PetscReal t, Vec u, Vec u_t, PetscReal a,
    // Mat A,
    //                       Mat B, void *ctx) -> PetscErrorCode {
    //   MoFEMFunctionBegin;
    //   CHKERR TsSetIJacobian(ts, t, u, u_t, a, A, B, ctx);
    //   TsCtx *ts_ctx = static_cast<TsCtx *>(ctx);
    //   auto &m_field = ts_ctx->mField;

    //   auto field_split_fun = [&]() -> PetscErrorCode {
    //     MoFEMFunctionBegin;
    //     DM dm;
    //     CHKERR TSGetDM(ts, &dm);

    //     SNES snes;
    //     CHKERR TSGetSNES(ts, &snes);
    //     KSP ksp;
    //     CHKERR SNESGetKSP(snes, &ksp);
    //     PC pc;
    //     CHKERR KSPGetPC(ksp, &pc);
    //     PetscBool is_pcfs = PETSC_FALSE;
    //     PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);

    //     if (is_pcfs) {

    //       IS is_lo;
    //       const MoFEM::Problem *problem_ptr;
    //       CHKERR DMMoFEMGetProblemPtr(dm, &problem_ptr);
    //       auto dofs = problem_ptr->getNumeredRowDofs();
    //       auto nb_local_dofs = problem_ptr->getNbLocalDofsRow();
    //       Tag tag;
    //       CHKERR m_field.get_moab().tag_get_handle("_ORDER", tag);

    //       std::vector<int> vec_is_lo;

    //       vec_is_lo.reserve(dofs->size());

    //       for (auto &dof : *(dofs)) {
    //         auto loc_idx = dof->getPetscLocalDofIdx();

    //         if (loc_idx < nb_local_dofs) {
    //           auto ent = dof->getEnt();
    //           int tag_order;
    //           CHKERR m_field.get_moab().tag_get_data(tag, &ent, 1,
    //                                                       &tag_order);
    //           auto o = dof->getDofOrder();

    //           bool correct_order = false;

    //           if (dof->getSpace() == L2) {

    //             correct_order = (o < tag_order);
    //           } else {

    //             correct_order = (o <= tag_order);
    //           }

    //           if (correct_order) {
    //             vec_is_lo.push_back(dof->getPetscGlobalDofIdx());

    //           }
    //         }
    //       }
    //       std::sort(vec_is_lo.begin(), vec_is_lo.end());
    //       CHKERR ISCreateGeneral(m_field.get_comm(), vec_is_lo.size(),
    //                              &*vec_is_lo.begin(), PETSC_COPY_VALUES,
    //                              &is_lo);
    //       // CHKERR ISView(is_lo, PETSC_VIEWER_STDOUT_SELF);
    //       CHKERR PCReset(pc);
    //       CHKERR PCSetOperators(pc, A, B);
    //       CHKERR PCSetFromOptions(pc);
    //       // CHKERR KSPReset(ksp);
    //       CHKERR PCFieldSplitSetIS(pc, NULL, is_lo);

    //       CHKERR PCSetUp(pc);
    //       // CHKERR PCSetFromOptions(pc);

    //       // CHKERR KSPSetFromOptions(ksp);
    //       // CHKERR PCReset(pc);

    //       CHKERR ISDestroy(&is_lo);
    //     }
    //     MoFEMFunctionReturn(0);
    //   };
    //   CHKERR field_split_fun();
    //   // And rest the same like in PreProc
    //   MoFEMFunctionReturn(0);
    // };
    // then you do
    // MoFEM::TsCtx *ts_ctx;
    // DMMoFEMGetTsCtx(dm, &ts_ctx);
    // CHKERR DMTSSetIJacobian(dm, ij_jacobian, ts_ctx);

    CHKERR TSSolve(ts, X);
MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::run_analysis(int nb_sp) {
  MoFEMFunctionBegin;

  global_error0 = 1;
  global_error1 = 1;
  post_error = 0;

  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-order", &g_max_order, PETSC_NULL);
  // set nb_species
  CHKERR setup_system(); // only once
  nb_species = nb_sp;
  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR add_fe("MASS1", "FLUX1"); // nb_species times
    if (nb_species == 2 || nb_species == 3) {
      CHKERR add_fe("MASS2", "FLUX2");
      if (nb_species == 3) {
        CHKERR add_fe("MASS3", "FLUX3");
      }
    }
  }

  CHKERR simple_interface->setUp();

    auto calculate_refinement = [&]() {
    MoFEMFunctionBegin;
    int o_rder = 1;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-tag_order", &o_rder, PETSC_NULL);
    CHKERR m_field.get_moab().tag_get_handle(
        "_ORDER", 1, MB_TYPE_INTEGER, data1->maxOrder, MB_TAG_CREAT | MB_TAG_DENSE, &o_rder);

    int r_ule = 1;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-tag_rule", &r_ule, PETSC_NULL);
    CHKERR m_field.get_moab().tag_get_handle(
        "_RULE", 1, MB_TYPE_INTEGER, data1->quadRule, MB_TAG_CREAT | MB_TAG_DENSE, &r_ule);
    MoFEMFunctionReturn(0);
  };

  CHKERR calculate_refinement();

  CHKERR set_blockData(material_blocks);

  CHKERR extract_bd_ents("ESSENTIAL", "NATURAL", "INTERNAL"); // nb_species times

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR extract_initial_ents(2, inner_surface1);
    CHKERR update_slow_rhs("MASS1", mass_values_ptr1);
    if (nb_species == 1) {
      vol_ele_slow_rhs->getOpPtrVector().push_back(new OpComputeSlowValue(
          "MASS1", data1, data1, data1, material_blocks));
    } else if (nb_species == 2 || nb_species == 3) {
      CHKERR extract_initial_ents(3, inner_surface2);
      CHKERR update_slow_rhs("MASS2", mass_values_ptr2);
      if (nb_species == 2) {
        vol_ele_slow_rhs->getOpPtrVector().push_back(new OpComputeSlowValue(
            "MASS1", data1, data2, data2, material_blocks));
      } else if (nb_species == 3) {
        CHKERR extract_initial_ents(4, inner_surface3);
        CHKERR update_slow_rhs("MASS3", mass_values_ptr3);
        vol_ele_slow_rhs->getOpPtrVector().push_back(new OpComputeSlowValue(
            "MASS1", data1, data2, data3, material_blocks));
      }
    }
  }
  natural_bdry_ele_slow_rhs->getOpPtrVector().push_back(
      new OpSetContrariantPiolaTransformOnEdge());

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR push_slow_rhs("MASS1", "FLUX1", data1); // nb_species times
    if (nb_species == 2 || nb_species == 3) {
      CHKERR push_slow_rhs("MASS2", "FLUX2", data2);
      if (nb_species == 3) {
        CHKERR push_slow_rhs("MASS3", "FLUX3", data3);
      }
    }
  }

  CHKERR update_vol_fe(vol_ele_stiff_rhs, data1);

  boost::shared_ptr<MatrixDouble> mass_grad_ptr1 =
      boost::shared_ptr<MatrixDouble>(data1, &data1->mass_grads);

  // vol_ele_stiff_rhs->getOpPtrVector().push_back(
  //     new OpSetInvJacH1ForFace(data1->inv_jac));

  // vol_ele_stiff_rhs->getOpPtrVector().push_back(
  //     new OpCalculateScalarFieldGradient<2>("MASS1", mass_grad_ptr1, MBTRI));

  

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR update_stiff_rhs("MASS1", "FLUX1", mass_values_ptr1,
                            flux_values_ptr1, mass_dots_ptr1, flux_divs_ptr1);

    CHKERR push_stiff_rhs("MASS1", "FLUX1", data1,
                          material_blocks); // nb_species times
    if (nb_species == 2 || nb_species == 3) {
      CHKERR update_stiff_rhs("MASS2", "FLUX2", mass_values_ptr2,
                              flux_values_ptr2, mass_dots_ptr2, flux_divs_ptr2);
      CHKERR push_stiff_rhs("MASS2", "FLUX2", data2, material_blocks);
      if (nb_species == 3) {
        CHKERR update_stiff_rhs("MASS3", "FLUX3", mass_values_ptr3,
                                flux_values_ptr3, mass_dots_ptr3,
                                flux_divs_ptr3);
        CHKERR push_stiff_rhs("MASS3", "FLUX3", data3, material_blocks);
      }
    }
  }

  CHKERR update_vol_fe(vol_ele_stiff_lhs, data1);

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR update_stiff_lhs("MASS1", "FLUX1", mass_values_ptr1,
                            flux_values_ptr1);
    CHKERR push_stiff_lhs("MASS1", "FLUX1", data1,
                          material_blocks); // nb_species times
    if (nb_species == 2 || nb_species == 3) {
      CHKERR update_stiff_lhs("MASS2", "FLUX2", mass_values_ptr2,
                              flux_values_ptr2);
      CHKERR push_stiff_lhs("MASS2", "FLUX2", data2, material_blocks);
      if (nb_species == 3) {
        CHKERR update_stiff_lhs("MASS3", "FLUX3", mass_values_ptr3,
                                flux_values_ptr3);
        CHKERR push_stiff_lhs("MASS3", "FLUX3", data3, material_blocks);
      }
    }
  }

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpCalculateGradL2<2>("MASS1", data1->inv_jac, data1->mass_grads));

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpPostError("ERROR2", ExactFunctionDot(), ExactFunctionLap(), data1,
                      material_blocks, post_error, m_field));


  // vol_ele_stiff_rhs->getOpPtrVector().push_back(
  //     new OpError(ExactFunction(), ExactFunctionLap(), ExactFunctionGrad(),
  //     data1, material_blocks, global_error0, global_error1, m_field));
  

  JumpData jump_data;

  // skeleton_fe->getOpPtrVector().push_back(
  //     new OpZeroDof("FLUX1", "ERROR2", m_field));
  skeleton_fe->getOpPtrVector().push_back(
      new OpSetContrariantPiolaTransformOnEdge());

  skeleton_fe->getOpPtrVector().push_back(new OpCalculateJumpErrors(
      "FLUX1", "MASS1", "ERROR2", m_field, jump_data, post_error));
  CHKERR set_integration_rule();

  dm = simple_interface->getDM();
  ts = createTS(m_field.get_comm());
  boost::shared_ptr<FaceEle> initial_mass_ele(new FaceEle(m_field));

  auto initial_mass_ele_rule = [&](int, int, int) -> int {
    auto fe_ent = initial_mass_ele->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(data1->quadRule, &fe_ent, 1,
                                           &quad_rule);

    return 2 * (quad_rule + 0);
  };
  initial_mass_ele->getRuleHook = initial_mass_ele_rule;

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR apply_IC("MASS1", inner_surface1, data1,
                    initial_mass_ele); // nb_species times
    if (nb_species == 2 || nb_species == 3) {
      CHKERR apply_IC("MASS2", inner_surface2, data1, initial_mass_ele);
      if (nb_species == 3) {
        CHKERR apply_IC("MASS3", inner_surface3, data1, initial_mass_ele);
      }
    }
  }
  CHKERR DMoFEMLoopFiniteElements(dm, simple_interface->getDomainFEName(),
                                  initial_mass_ele);

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR apply_BC("FLUX1"); // nb_species times
    if (nb_species == 2 || nb_species == 3) {
      CHKERR apply_BC("FLUX2");
      if (nb_species == 3) {
        CHKERR apply_BC("FLUX3");
      }
    }
  }



  CHKERR loop_fe();                          // only once
  post_proc->generateReferenceElementMesh(); // only once

  post_proc->getOpPtrVector().push_back(new OpCalculateJacForFace(data1->jac));
  post_proc->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(data1->inv_jac));
  post_proc->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());

  post_proc->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformFace(data1->jac));

  post_proc->getOpPtrVector().push_back(
      new OpSetInvJacHcurlFace(data1->inv_jac));

  post_proc->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("MASS1", mass_values_ptr1, MBTRI));

  post_proc->getOpPtrVector().push_back(
      new OpCalculateHdivVectorField<3>("FLUX1", flux_values_ptr1));

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR post_proc_fields("MASS1", "FLUX1");
    post_proc->addFieldValuesPostProc("ERROR");
    post_proc->addFieldValuesPostProc("ERROR2");
    if (nb_species == 2 || nb_species == 3) {
      CHKERR post_proc_fields("MASS2", "FLUX2");
      if (nb_species == 3) {
        CHKERR post_proc_fields("MASS3", "FLUX3");
      }
    }
  }

 
  
  maxError = 0;
  max_post_error_fe->getOpPtrVector().push_back(new OpMaxError("ERROR2", maxError));
  monitor_ptr = boost::shared_ptr<Monitor>(
      new Monitor(cOmm, rAnk, dm, ts, max_post_error_fe, post_proc, global_error0,
                  global_error1, post_error, maxError, m_field, data1->maxOrder, data1->quadRule)); // nb_species times
  CHKERR output_result();          // only once
  CHKERR solve();                  // only once
  MoFEMFunctionReturn(0);
}

int main(int argc, char *argv[]) {
  const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, param_file, help);
  try {
    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    
    int nb_species = 1;
    RDProblem reac_diff_problem(core);
    CHKERR reac_diff_problem.run_analysis(nb_species);
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}